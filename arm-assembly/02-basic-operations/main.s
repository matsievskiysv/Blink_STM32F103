	.syntax unified		@ use unified assembly syntax

	.section .isr_vector,"a" @ reset vector section
	.word _stack_start	@ initial stack address
	.word _reset+1		@ entry point
	@@ Note: last address bit selects processor mode
	@@ 0 - ARM mode
	@@ 1 - Thumb mode


	.section .data		@ data section start
line:		.asciz "hello world\n\0"
	.align 4 		@ align to word boundary
uart_base:	.word 0x4000c000 @ UART port

	.section .text		@ text section start
_reset:
	.global _start		@ make _start visible
_start:				@ load constants to registers
	ldr r0, =line		@ (char *)
	ldr r1, =uart_base	@ (size_t *)
	ldr r1, [r1]		@ holds address of UART port
	add r2, r1, #0x18	@ holds address of UART reset port

loop:				@ print out line

0:		      		@ wait for UART ready
	ldrb ip, [r2]		@ read UART status
	and ip, #0b10001000	@ select busy flag
	cmp ip, #0b10001000	@ check if busy
	beq 0b			@ loop if busy
	nop
	@@ Note: it is recommended to put no-op command after branching

	ldrb ip, [r0]		@ load character
	cmp ip, #0		@ check null character
	beq wait		@ break loop if found string terminator
	nop

	strb ip, [r1]		@ send it over UART
	add r0, r0, #1		@ increment char pointer
	b loop			@ repeat
	nop			@ no-op command

wait:				@ endless loop. echo UART messages
	ldrb ip, [r2]		@ read UART status
	and ip, #0b10011000	@ select busy, RX empty and TX empty flags
	cmp ip, #0b10000000	@ check if transmission is done that there's incoming data
	bne 0f			@ skip printing
	nop

	ldrb ip, [r1]		@ read UART
	strb ip, [r1]		@ echo received char to UART
0:
	b wait			@ endless loop
	nop

	.end
