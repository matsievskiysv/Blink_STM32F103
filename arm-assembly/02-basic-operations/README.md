# Introduction

## Processors

Processors execute predefined set of commands sequentially (from the user point of view). Set of commands, known to processor is called *instruction set*. Processor internal design is called *architecture*. Processors with the same architecture are have the same instruction set.
Processor commands mutate memory, that is connected to the processor. Processors have different bus width, which affects their ability to access memory. Bus width may also be called processor *word size* because processor registers have the same capacity as a bus.
Special memory regions inside processor are called registers. Most of the operations use register values. Processor registers have names and are accessed by name. For example, ARM processors registers are named:

| Register | Alias | Description |
|:--|:--|---|
| `R0` | `–` | General purpose |
| `R1` | `–` | General purpose |
| `R2` | `–` | General purpose |
| `R3` | `–` | General purpose |
| `R4` | `–` | General purpose |
| `R5` | `–` | General purpose |
| `R6` | `–` | General purpose |
| `R7` | `–` | Holds Syscall Number |
| `R8` | `–` | General purpose |
| `R9` | `–` | General purpose |
| `R10` | `–` | General purpose |
| `R11` | `FP` | Frame Pointer |
| `R12` | `IP` | Intra Procedural Call |
| `R13` | `SP` | Stack Pointer |
| `R14` | `LR` | Link Register |
| `R15` | `PC` | Program Counter |
| `CPSR` | `–` | Current Program Status Register |

Processors may store memory in different way. Data **bytes** may be stored in memory cells from least significant byte to the most significant byte. This format is called *little-endian*.

![little-endian.png](../documents/Little-Endian.png)

Alternatively, data bytes may be stored in reversed order. This format is called *big-endian*.

![big-endian.png](../documents/Big-Endian.png)

There is only a small number of registers in each processor. In order to store data processors use *random access memory* or *RAM*. Usual workflow includes fetching data form RAM to registers, executing a command over these data, storing results to RAM.
RAM data is accessed by address. Available address range is called *address space*.
RAM is accessed by address but not all addresses point to RAM. For example, for 32 bit processor address word may express $2^{32} = 4 [Gb]$ addresses. Only some of these addresses point to the storage cells in RAM. Some regions of address space are reserved, some pointing to read only memory or *ROM* and some point to input/output or *I/O* registers.
*I/O* registers are the special registers in *memory mapped I/O* model, which point to the internal device registers. For example, some of addresses in address space belong to general purpose input output or *GPIO* device. By setting appropriate bits of predefined addresses user may control GPIO ports: set data direction, output voltage level (high or low) etc.

## Emulators

Processor functionality may be emulated by catching user issued commands and translating them for the current architecture. Programs with such functionality are called *emulators* or *virtual machines*.

## ARM Assembly. THUMB instruction set

Assembly is the lowest level of human communication with processor. Each assembly command mapped one-to-one to the binary processor instructions. *Assembler* is a program that makes this conversion from human readable text instructions to binary machine readable commands.

| Instruction |	Description |
|:--|:--|
| `ADD` | Addition |
| `SUB` | Subtraction |
| `MOV` | Move data |
| `MVN` | Move and negate |
| `MUL` | Multiplication |
| `EOR` | Bitwise `XOR` |
| `ORR` | Bitwise `OR` |
| `AND` | Bitwise `AND` |
| `LDR` | Load |
| `LDM` | Load Multiple |
| `STR` | Store |
| `STM` | Store Multiple |
| `LSL` | Logical Shift Left |
| `LSR` | Logical Shift Right |
| `ASR` | Arithmetic Shift Right |
| `ROR` | Rotate Right |
| `PUSH` | Push on Stack |
| `POP` | Pop off Stack |
| `CMP` | Compare |
| `B` | Branch |
| `BL` | Branch with Link |
| `BX` | Branch and eXchange |
| `BLX` | Branch with Link and eXchange |
| `SWI`/`SVC` | System Call |

This list is not complete. For additional instructions, examples and notes consult ARM manual.

### Labels

Code lines may be given names called labels. For example:
```assembly
mylabel:
	MOV r1, r2
myotherlabel:
	MOV r1, r2
```

Here we declared two labels `mylabel` and `myotherlabel`. We can use labels for branching.

### Data movement

```assembly
<op> <rd>, <r1>
```
Move data form registry `r1` to `rd`.

`<op>` is one of the following:

- `MOV` -- move
- `MVN` -- move and perform bitwise NOT on data
- `NEG` -- move and multiply by $-1$

```assembly
<op> <rd>, #<const>
```

Store constant `<const>` in range `0-255` to `rd`. `<op>` is one of the above.

### Arithmetic

```assembly
<op> <rd>, <r1>, <r2>
```
Perform operation `<op>` (`ADD` for addition or `SUB` for subtraction) on `<r1>` and `<r2>` and store result to `<rd>`.

```assembly
<op> <rd>, <r1>, #<const>
```
Perform operation `<op>` (`ADD` or `SUB`) on `<r1>` and constant `<const>` in range $\pm 7$ and store result to `<rd>`.

```assembly
<op> <rd>, #<const>
```
Perform operation `<op>` (`ADD` or `SUB`) on `<rd>` and constant `<const>` in range $\pm 255$ and store result to `<rd>`.

```assembly
MUL <rd>, <r1>
```
Multiply `<rd>` by `<r1>` and store result to `<rd>`.

### Logical operations

```assembly
<op> <rd>, <r1>
```
Perform operation `<op>` (`AND`, `ORR`, `EOR`, `BIC`) on `<rd>` and `<r1>` and store result to `<rd>`.

- `AND` -- logical AND
- `ORR` -- logical OR
- `EOR` -- logical exclusive OR
- `BIC` -- AND NOT

### Branching

Branching is an equivalent of `if` - `else` structures in high level programming languages.

```assembly
<op> <r1>, <r2>
```

Compare `<r1>` and `<r2>`. Result of operation updates flags in `CPSR` register.

`<op>` is one of the following:

- `CMP` -- compare
- `CMN` -- compare negative

```assembly
CMP <r1>, #<const>
```

Compare `<r1>` and constant `<const>` in range $0-255$ Result of operation updates flags in `CPSR` register.

```assembly
B<cond> <label>
```
Branch to `<label>` based on condition and flags in register `CPSR`.

Label must be within:

- from $–252$ to $258$ bytes of the current instruction, if `<cond>` is used
- $\pm 2 [KB]$ if the instruction is unconditional

| Suffix | Flags | Meaning |
|:--|:--|:--|
| `EQ` | `Z` set | Equal |
| `NE` | `Z` clear | Not equal |
| `GE` | `N` and `V` the same | Signed `>=` |
| `LT` | `N` and `V` different | Signed `<` |
| `GT` | `Z` clear, and `N` and `V` the same | Signed `>` |
| `LE` | `Z` set, or `N` and `V` different | Signed `<=` |

```assembly
BL <label>
```
Long branch to `<label>`.

### Memory access

#### Immediate offset

```assembly
<op><type> <rd>, [<rs>, #<offset>]
```
Perform operation `<op>` (`LDR` to load or `STR` to store) from `<rs>` to `<rd>` with offset `<offset>`. `<type>` is empty for full word, `H` for half-word, `B` for byte.

#### Register offset

```assembly
<op> <rd>, [<rs>, <ro>]
```
Perform operation `<op>` from `<rs>` to `<rd>` with offset from `<ro>`.

`<op>` is one of the following:

| Instruction | Description |
|:--|:--|
| `LDR` | Load register, 4-byte word |
| `STR` | Store register, 4-byte word |
| `LDRH` | Load register, 2-byte unsigned halfword |
| `LDRSH` | Load register, 2-byte signed halfword |
| `STRH` | Store register, 2-byte halfword |
| `LDRB` | Load register, unsigned byte |
| `LDRSB` | Load register, signed byte |
| `STRB` | Store register, byte |

### Stack

Stack operations allow to store data in memory in first in - last out or *FILO* queue. This is essential for adding support for high level languages that rely on stack for function calls and local variable implementation.

```assembly
<op> {<rlist>}
```
Perform operation `<op>` (`PUSH` to add and `POP` to remove) on list of registers `<rlist>`.

## Sections and Linking scripts

Assembly programs are split in chunks called sections. There are four general sections:

- `text` for assembly instructions
- `data` for variables
- `rodata` for constants
- `bss` for uninitialized variables

Other sections may be defined.

Section names are used to place chunks of binary code inside address space. This process is controlled by *linker scripts*.
Linker scripts are used as an input by *linkers* - programs that resolve human readable variable and label names and other expressions with the actual addresses in address space.
Linker scripts contain memory map definitions with region names, sizes and attributes.

```linkerscript
MEMORY {
	RAM   (rwx) : ORIGIN = 0x20000000, LENGTH = 20k
	FLASH (rx)  : ORIGIN = 0x08000000, LENGTH = 64k
}
```

These region names are used for placing sections.

```linkerscript
SECTIONS {
	.text : { *(.text) } > RAM
	.data : { *(.data) } > RAM
	.bss  : { *(.bss)  } > RAM
}
```

## Emulating and debugging

Quick emulator or `QEMU` is used for emulating ARM based processor. Emulated `lm3s811evb` has address `0x4000c000` mapped to universal asynchronous receiver/transmitter bus or `UART`. Since this bus is connected to TeleTypewriter or `TTY`, received data will appear on the screen.

In order to run the emulator, issue the command:
```bash
make qemu
```

And to attach debugger to it:
```bash
make debug
```

# Programming

## Template

### Code

Symbol `@` is used for commenting.

```assembly
	.syntax unified		@ 1

	.section .isr_vector,"a" @ 2
	.word _stack_start	@ 3
	.word _reset+1		@ 4

	.section .data		@ 5
	@@ your constants go here
	@@ your constants go here
line:		.asciz "hello world" @ 6
	.align 4 		@ 7

	.section .bss		@ 8
	@@ your varables go here
	@@ your varables go here

	.section .text		@ 9
_reset:				@ 10
	.global _start		@ 11
_start:				@ 12
	@@ your code goes here
	@@ your code goes here
	@@ your code goes here

wait:				@ 13
	b wait			@ 14
	nop			@ 15

	.end
```

Code explanation line-by-line:

1. Select new `unified` syntax (as opposed to the old `divided`)
2. Beginning of `.isr_vector` section. Also called *reset vector*
3. Initialization of processor `sp` registry
4. Location of function, which will be called after processor reset. Last bit indicates instruction set. `0` -- for ARM mode, `1` -- for Thumb mode. Therefore, `_reset+1` sets the reset function to be in Thumb mode
5. Beginning of section `.data`. It is used for constant **definition**
6. Definition of the constant string
7. Align data to `word` boundary. Most processors require memory data alignment. It means, that variables cannot be located in the arbitrary locations. Most of the processors require data to start at `word` boundary. For 32 bit processors word size is 4 bytes long. Explicit data alignment is not required after integer and float values since their size is multiple of `word` size
8. Beginning of section `.bss`. It is used for variable **declaration**
9. Beginning of section `.text`. It is used for program instructions
10. `_reset` label was chosen earlier to be processor *reset handler*. Code execution will start from this point
11. Define `_start` to be a global function
12. Beginning of `_start` function. It is conventional to start your code from the `_start` function
13. Beginning of `wait` function. It is an infinite loop which keeps processor busy at the end of the program
14. Unconditional branch always jumps to `wait` function
15. It is mandatory to have some code after the branch. `nop` is *no operation* placeholder instruction

### Build & run

Copy code to the file [main.s](./main.s).

Compile code into binary:
```bash
make
```

Start QEMU emulator:
```bash
make qemu
```
This command will block all user input in the terminal window. Open new terminal window for the following commands.

Attach debugger to emulator:
```bash
make debug
```

To stop QEMU either close its window or enter <kbd>Ctrl+c</kbd> in its terminal.

### Exercises

1. Compile and run program. Use debugger

## Basic registry manipulations

### Code

The following code goes inside `_start` function in template code.

```assembly
_start:
	mov r0, #55		@ 1
	mov r1, #0xac		@ 2
	mov ip, r1		@ 3
	add r2, r0, r1		@ 4
	add r2, r2, #1		@ 5
	add r3, r2, #-1		@ 6
	sub r4, r2, #1		@ 7
	mul r0, r1		@ 8
	and r0, #0xf 		@ 9
	eor r1, r0 		@ 10
```

Code explanation line-by-line:

1. Move constant `55` to registry `r0`
2. Move hexadecimal constant `0xac` to registry `r1`
3. Copy contents of registry `r1` to registry `ip`
4. Add contents of registers `r0` and `r1` and store result to `r2`
5. Add 1 to content of register `r2` and store result to `r2`
6. Add -1 to content of register `r2` and store result to `r3`
7. Subtract 1 from content of register `r2` and store result to `r4`. Result is the same as for the instruction above
8. Multiply contents of registers `r0` and `r1` and store result to `r0`
9. Perform logical AND operation on content of register `r0` and constant `0x0f` and store result to `r0`
10. Perform logical exclusive OR operation on contents of registers `r1` and `r0` and store result to `r1`

### Exercises

1. Set registers to the following values:
   1. `r0` to `0x5`
   1. `r1` to `0x2`

   Compute the following:
   1. Set `r2` to sum of `r0` and `r1`
   1. Set `r3` to difference between `r0` and `r1`
   1. Set `r4` to product of `r0` and `r1`
   1. Set `r5` to factorial of `r0`

## Branching

### Code

The following code goes inside `_start` function in template code.

```assembly
_start:
	mov r0, #1		@ 1
	mov r1, #1
	mov r2, #0
	mov r3, #10
loop:				@ 2
	cmp r0, #5		@ 3
	it eq			@ 4
	bleq fifth		@ 5
	nop			@ 6
	mul r1, r0		@ 7
	add r0, r0, #1		@ 8
	cmp r0, r3		@ 9
	ble loop		@ 10
	nop
	b wait			@ 11
	nop
fifth:
	mov r2, #5		@ 12
	bx lr			@ 13
```

This code implements simple loop. Register `r0` acts as a counter for the loop, register `r3` holds loop condition. Counter `r0` increments each iteration. If counter reaches `5`, it would be stored in register `r2` using instructions `bl` (*branch-link*) and `bx` (*branch-exchange*). Register `r1` holds computed factorial of argument `r0`.

Code explanation line-by-line:

1. Initialize registers with values
2. Main loop label
3. Compare contents of register `r0` with `5`
4. Indicates that the following operation will be conditional
5. Jump to label `fifth` if `r0` == `5`. Save address of the next instruction in register `lr` in order to return from jump
6. `nop` (*no operation*) instruction should be used after branching
7. Compute factorial and store to register `r1`
8. Increment counter `r0`
9. Check that `r0` is not greater then `r3`
10. Jump to `loop` label if `r0` <= `r3`
11. Jump to `wait` unconditionally
12. Store `5` to register `r2`
13. Return to the loop by jumping to the address stored in register `lr`

### Exercises

1. Compute factorial of `0x5`, `0x7` and `0xa` and store results in registers `r0`, `r1` and `r2` respectively.


## Memory operations

### Code

The following code goes inside `.data` section in template code.
```assembly
	.section .data
x:	.word 0x5
y:	.word 12345
```

This code defines two constants and assigns label `x` to the first one and label `y` to the second one.

The following code goes inside `.bss` section in template code.
```assembly
	.section .bss
a:	.space 0x4
b:	.space 0x4
```

This code declares two variables and assigns label `a` to the first one and label `b` to the second one. This code only marks addresses of the variables and does not assigns any value to them.
This is due to the fact that `.bss` section resides in *RAM* which is volatile and does not contain any meaningful data upon reboot.

The following code goes inside `_start` function in template code.

```assembly
_start:
	ldr r0, =x		@ 1
	ldr r0, [r0]	@ 2
	ldr r1, =y		@ 3
	ldr r1, [r1]	@ 4
	ldr ip, =a		@ 5
	str r0, [ip]		@ 6
	ldr ip, =b		@ 7
	str r1, [ip]		@ 8
	add r2, r0, r1		@ 9
	add ip, ip, #4		@ 10
	str r2, [ip]		@ 11
	push {r0, r1, r2}	@ 12
	pop {r2, r1, r0}	@ 13
```

Load constants into registers, compute their sum, store all three values into memory and push them onto stack.
Code explanation line-by-line:

1. Load **address** of the variable `x` into register `r0`
2. Load variable `x` into register `r0`
3. Load **address** of the variable `y` into register `r1`
4. Load variable `y` into register `r1`
5. Load **address** of the variable `a` into register `ip`
6. Store data from register `r0` into memory by address from register `ip`
7. Load **address** of the variable `b` into register `ip`
8. Store data from register `r1` into memory by address from register `ip`
9. Add contents of registers `r0` and `r1` and store result to `r2`
10. Increment value in register `ip` by `0x04`
11. Store data from register `r2` into memory by address from register `ip`
12. Push contents of registers `r0`, `r1` and `r2` onto stack
13. Pop stack into `r0`, `r1` and `r2`

### Exercises

1. Define constant `x` in `.data` section of the program. Compute factorial of variables form 1 to `x` and push results onto stack.
1. Define constant `x` in `.data` section of the program. Declare array in `.bss` section. Compute factorial of variables form 1 to `x` and store to `.bss` section in reversed order.
1. Read two space separated unsigned integer numbers from `UART` port. User input ends with newline character. Write to the `UART` port:
	1. `LESS` if the first number if lesser then the second one
	1. `MORE` if the first number if greater then the second one
	1. `EQUAL` if numbers are equal


# References

Pictures *Little Endian* and *Big Endian* were taken from <https://en.wikipedia.org/wiki/Endianness>

<https://azeria-labs.com/writing-arm-assembly-part-1/>

<http://infocenter.arm.com/help/topic/com.arm.doc.dui0068b/DUI0068.pdf>
