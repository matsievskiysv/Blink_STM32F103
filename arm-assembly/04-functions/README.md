# Introduction

*Application binary interface* (ABI) is a function calling convention. Assembly does not impose any restrictions on calling subroutines. However, it is necessary to have a set of rules for function argument passing. This allows to use in one program code written in different languages and compiled with different compilers. There are many different ABIs for one architecture.

## AAPCS

This project uses `aapcs` ABI. It uses both registers and stack to pass function arguments, which is more beneficial compared to pure stack ABIs.
Here only simplest function call case is described. If you need to pass something other then `word` as an argument or to use vararg function, read full AAPCS manual.

| Register | Role |
|:--|:--|
| `pc` | Program counter |
| `lr` | Link register |
| `sp` | Stack pointer |
| `ip` | Intra-Procedure-call Scratch register |
| `r11` | Variable register |
| `r10` | Variable register |
| `r9` | Platform register |
| `r8` | Variable register |
| `r7` | Variable register |
| `r6` | Variable register |
| `r5` | Variable register |
| `r4` | Variable register |
| `r3` | Argument/scratch register |
| `r2` | Argument/scratch register |
| `r1` | Argument/result/scratch register |
| `r0` | Argument/result/scratch register |

First four function arguments are passed using in registers `r0`-`r3`.
All other function arguments are passed using stack. After subroutine completion, calculation results reside in `r0` and `r1` registers. Call arguments on stack must be cleared by the caller.
Registers `r0`-`r3` are not preserved during subroutine call, therefore caller must assume that their contents will change. On the other hand, registers `r4`-`r8`, `r10`-`r11` are preserved after function call. Caller may rely on them in order to store local variables. Keep in mind that caller function itself must obey ABI rules saving contents of these registers at the beginning of the calculations and restoring them at the end.

## C inline assembly

C allows to add inline assembly code via `asm` statement.

```C
asm asm-qualifiers (AssemblerTemplate : OutputOperands [ : InputOperands [ : Clobbers ] ])
```

Example of inline assembly usage is shown below:

```C
unsigned int
inline_sum(unsigned int x1, unsigned int x2)
{
	unsigned int output = 0, temp = 0;
	asm("sub %[t], %[arg1], %[arg2]\n\t"
	    "mul %[arg1], %[t]\n\t"
	    "add %[result], %[arg1], %[arg2]\n\t"
	    : [result]"=r"	(output),
	      [t]"+r"		(temp),
	      [arg1]"+r"	(x1)
	    : [arg2]"r"	(x2)
	    :);
	return output;
}
```

Assembly code instructions are passed as first `asm` argument as string. Registers are written using special syntax `%[reg]` where `reg` is an arbitrary register name. This function maps C variables to assembly registers. Later these registers are listed as input, output or clobber registers. Input and output registers contain inline assembly input and output, clobber registers are scratch registers with temporary data.

# Programming

## Template

### Code

```assembly
	.syntax unified		@ use unified assembly syntax

	.section .isr_vector,"a" @ reset vector section
	b _reset		@ entry point
	b _reset 		@ Reset: relative branch allows remap
	b .			@ Undefined Instruction
	b .			@ Software Interrupt
	b .			@ Prefetch Abort
	b .			@ Data Abort
	b .			@ Reserved
	b .			@ IRQ
	b .			@ FIQ

	.section .data
	@@ your constants go here
	@@ your constants go here

	.section .bss
	@@ your varables go here
	@@ your varables go here

	.section .text		@ text section start

	@@ void
	@@ _init_stack(void)
	@@
	@@ initialize stack
	.extern _stack_start
_init_stack:
	ldr sp, =_stack_start
	bx lr
	nop

	@@ void
	@@ _enable_fpu(unsigned int i)
	@@
	@@ enable fpu
	@@ i = 0 for single precision
	@@ i = 1 for double precision
_enable_fpu:
	mrc p15, 0, ip, c1, c0, 2
	cmp r0, #0
	ite eq
	orreq ip, ip, #0x300000 @ single precision
	orrne ip, ip, #0xC00000 @ double precision
	mcr p15, 0, ip, c1, c0, 2
	mov ip, #0x40000000
	fmxr fpexc, ip
	bx lr
	nop

_reset:
	.global _start		@ make _start visible
	.func _start
_start:
	bl _init_stack		@ initialize stack
	nop

	mov r0, #0 		@ fpu single precision
	bl _enable_fpu		@ initialize fpu
	nop

	@@ your code goes here
	@@ your code goes here
	@@ your code goes here

	b wait			@ call wait function
	nop			@ no-op command
	.endfunc

	@@ void
	@@ wait(void)
	@@
	@@ does not return
	.func wait
wait:
	b wait			@ endless loop
	nop
	.endfunc

	@@ your function definitions go here
	@@ your function definitions go here
	@@ your function definitions go here

	.end
```

### Exercises

1. Compile and run program. Use debugger

## Call function with less then 5 arguments

The following code goes inside _start function in template code.

```assembly
	mov r0, #1		@ 1
	mov r1, #2
	mov r2, #3
	mov r3, #4
	bl fun_4args		@ 2
	nop
```

Code explanation line-by-line:

1. Load function arguments to registers `r0`-`r3`
2. Call function

### Exercises

1. Refactor your previous code using AAPCS ABI

## Call function with more then 4 arguments

The following code goes inside _start function in template code.

```assembly
	mov r0, #1		@ 1
	mov r1, #2
	mov r2, #3
	mov r3, #4
	add sp, sp, #8		@ 2
	mov ip, #5		@ 3
	str ip, [sp]
	mov ip, #6
	str ip, [sp, #4]
	bl fun_6args		@ 4
	sub sp, sp, #8	@ 5
	nop
```

Code explanation line-by-line:

1. Load function arguments to registers `r0`-`r3`
2. Allocate stack space for two more arguments
3. Copy last two arguments to stack
4. Call function
5. Pop function call arguments from stack

### Exercises

1. Refactor your previous code using AAPCS ABI
