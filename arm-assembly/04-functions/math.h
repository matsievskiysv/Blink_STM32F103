unsigned int factorial(unsigned int x);

unsigned int fun_4args(unsigned int x1, unsigned int x2, unsigned int x3, unsigned int iter);

unsigned int fun_6args(unsigned int x1, unsigned int x2, unsigned int x3, unsigned int x4, unsigned int x5,
		       unsigned int x6);

unsigned int fun_float(unsigned int x1, unsigned int x2);

unsigned int extern_sum(unsigned int x1, unsigned int x2);

unsigned int inline_sum(unsigned int x1, unsigned int x2);
