#include "main.h"

unsigned int
factorial(unsigned int x)
{
	unsigned int acc = 1;
	if (x > 1)
		acc = factorial(x - 1);
	return acc * x;
}

unsigned int
fun_4args(unsigned int x1, unsigned int x2, unsigned int x3, unsigned int iter)
{
	unsigned int sum = 0;
	for (unsigned int i = 0; i < iter; i++) {
		sum += x1 + x2;
		if ((i & 0x2) == 0x2) {
			sum *= x3;
		}
	}
	return sum;
}

unsigned int
fun_6args(unsigned int x1, unsigned int x2, unsigned int x3, unsigned int x4, unsigned int x5, unsigned int x6)
{
	return x1 + x2 + x3 + x4 + x5 + x6;
}

unsigned int
fun_float(unsigned int x1, unsigned int x2)
{
	float x = (float) x1 / (float) x2;
	return (unsigned int) x;
}

unsigned int
extern_sum(unsigned int x1, unsigned int x2)
{
	return as_sum_fac(x1, x2);
}

unsigned int
inline_sum(unsigned int x1, unsigned int x2)
{
	unsigned int output = 0, temp = 0;
	asm("sub %[t], %[arg1], %[arg2]\n"
	    "mul %[arg1], %[t]\n"
	    "add %[result], %[arg1], %[arg2]\n"
	    : [result] "=r"(output), [t] "+r"(temp), [arg1] "+r"(x1)
	    : [arg2] "r"(x2)
	    :);
	return output;
}
