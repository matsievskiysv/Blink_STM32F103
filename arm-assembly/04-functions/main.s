	.syntax unified		@ use unified assembly syntax

	.section .isr_vector,"a" @ reset vector section
	b _start		@ entry point
	b _start 		@ Reset: relative branch allows remap
	b .			@ Undefined Instruction
	b .			@ Software Interrupt
	b .			@ Prefetch Abort
	b .			@ Data Abort
	b .			@ Reserved
	b .			@ IRQ
	b .			@ FIQ

	.section .text		@ text section start

	@@ void
	@@ _init_stack(void)
	@@
	@@ initialize stack
	.extern _stack_start
_init_stack:
	ldr sp, =_stack_start
	bx lr
	nop

	@@ void
	@@ _enable_fpu(unsigned int i)
	@@
	@@ enable fpu
	@@ i = 0 for single precision
	@@ i = 1 for double precision
_enable_fpu:
	mrc p15, 0, r1, c1, c0, 2
	cmp r0, #0
	ite eq
	orreq r1, r1, #0x300000 @ single precision
	orrne r1, r1, #0xC00000 @ double precision
	mcr p15, 0, r1, c1, c0, 2
	mov r1, #0x40000000
	fmxr fpexc, r1
	bx lr
	nop

	.global _start		@ make _start visible
	.func _start
_start:
	bl _init_stack		@ initialize stack
	nop

	mov r0, #0 		@ fpu single precision
	bl _enable_fpu		@ initialize fpu
	nop

	mov r0, #8
	bl factorial		@ call function
	nop
	mov r4, r0		@ save return value to r4

	mov r0, #1		@ set arguments
	mov r1, #2
	mov r2, #3
	mov r3, #4
	bl fun_4args		@ call function
	mov r5, r0		@ save return value to r5
	nop

	mov r0, #1		@ set arguments
	mov r1, #2
	mov r2, #3
	mov r3, #4
	add sp, sp, #8		@ allocate stack space
	mov ip, #5
	str ip, [sp]
	mov ip, #6
	str ip, [sp, #4]
	bl fun_6args		@ call function
	mov r6, r0		@ save return value to r6
	sub sp, sp, #8		@ pop stack
	nop

	mov r0, #15
	mov r1, #7
	bl fun_float		@ call function
	mov r7, r0		@ save return value to r7
	nop

	mov r0, #7
	mov r1, #6
	bl extern_sum		@ call function
	mov r8, r0		@ save return value to r8
	nop

	mov r0, #8
	mov r1, #6
	bl inline_sum		@ call function
	mov r10, r0		@ save return value to r10
	nop

	b wait			@ call wait function
	nop			@ no-op command
	.endfunc

	@@ void
	@@ wait(void)
	@@
	@@ does not return
	.func wait
wait:
	b wait			@ endless loop
	nop
	.endfunc

	@@ unsigned int
	@@ as_sum_fac(unsigned int x1, unsigned int x2)
	@@
	@@ sum of two factorials
	.global as_sum_fac	@ make _start visible
	.func as_sum_fac
as_sum_fac:
	push {fp, lr}		@ save registers (need to be aligned to 0x8)
	add fp, sp, #8		@ save frame
	push {r4, r5} 		@ save registers
	mov r4, r1		@ save r1
	bl factorial		@ compute factorial
	nop
	mov r5, r0		@ save first factorial
	mov r0, r4		@ set second factorial argument
	bl factorial 		@ compute factorial
	nop
	add r0, r0, r5
	pop {r4, r5} 		@ restore registers
	pop {fp, pc}		@ return from function
	nop
	.endfunc

	.end
