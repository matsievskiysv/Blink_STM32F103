# Installation

To install tool-chain on [Debian](https://www.debian.org/) system, issue command
```bash
sudo apt-get install build-essential gcc-arm-none-eabi gdb-multiarch openocd qemu-system-arm
```

# C program structure

The structure of project folders is this:
```
.
├── include
│   └── foo.h
├── main.c
├── Makefile
└── src
    └── foo.c
```

Here `main.c` is a main project source file.
```C
#include <stdio.h>
#include <foo.h>

int
main(void)
{
	printf("hello world\n");
	printf("foo(3) = %d; bar(7) = %.3f\n", foo(3), bar(7));
	return 0;
}
```
It contains a function `main` which is used as an entry point. In it, functions `foo` and `bar` are used. They are loaded from `foo.h` header.
Header files describe functions to the compiler and are used for loading functions from other source files. Source and header files are usually go in pairs. It is common to use separate folders for source and header files. Here they are called `src` and `include`.

# Native compiler

Let's try to build this program manually. For this we will use **native compiler** which compiles program to binary which may be run on current machine architecture. We will begin with `foo.c` source file:
```C
int
foo(int a)
{
	int b = a*2;
	return b;
}

float
bar(int a)
{
	float b = 1 / (float)a;
	return b;
}
```

It contains two simple functions. In order to compile it, issue:

```bash
gcc -g -c -o foo.o src/foo.c
```

Here, `gcc` is a [Gnu C Compiler](https://gcc.gnu.org/). Flag `-g` adds debugging information to program. `-c` indicates that current file does not have `main` function in it. `-o <name>` flag tells compiler to use `<name>` as a name of binary created. Default value is `a.out` Finally, the last argument is a location of the source file.
Now we need to create an **executable**. Executable is a binary file which we can actually run on the computer.

Simply issuing command
```bash
gcc -g -o main src/main.c
```
will fail with error
```
fatal error: foo.h: No such file or directory
```
We need to point compiler to `foo.h` and `foo.o` files.

Correct command is
```bash
gcc -g -I include -o main foo.o src/main.c
```

Now program may be called from shell
```bash
./main
```

# Makefile

Compiling manually gets tedious really fast. [GNU make](https://www.gnu.org/software/make/) is used to automate this process via Makefiles.
Makefiles are script files that describe how to build a program. Each Makefile may have more than one rule in it.
For example, the following code snippet is a Makefile rule for cleaning project directory.
```Makefile
clean:
	rm -f $(OBJS)
	rm -f $(BIN)
	rm -f *.log
	rm -f *~
```

> Tabs are used for indenting Makefiles.
{.is-warning}

After typing
```bash
make clean
```
`make` program executes `rm` commands.

Rewriting compiler commands we may look something like this
```Makefile
CC := gcc
CCFLAGS := -g
LDFLAGS := -I include

foo.o: src/foo.c
	$(CC) $(CCFLAGS) -c -o $@ $^

main: foo.o main.c
	$(CC) $(LDFLAGS) $(CCFLAGS) -o $@ $^
```
Real world Makefiles use a lot of variables and generic rules.
> `make` executes rules lazily. If rule dependency file has not been changed, rule will not be rerun.
{.is-info}

# Debugging

`gdb` stands for [GNU debugger](https://www.gnu.org/software/gdb/).
Running Makefile rule
```Makefile
make debug
```
will build program and run it under `gdb`.

One may add breakpoints.
```gdb
break foo
break foo.c:5
```
This will halt program as soon as function `foo` will be called or
when we will step into 5th line of `foo.c` file.
Most commonly used actions `next`, `step` and `stepi` control line-by-line execution of the program.

In order to run program, issue
```gdb
run
```
if program was not started, and
```gdb
continue
```
if it was halted.

Values of variables may be examined by issuing command
```gdb
print <name>
```
or changed
```gdb
set variable <name> = <value>
```
where `<name>` is a variable name in current scope and `<value>` is a new value.

To print variable value in hexadecimal or binary forms use commands
```gdb
print/x <name>
print/t <name>
```

Just like in C, variable address may be examined using command
```gdb
print &<name>
```
It is also possible to interpret variable as a pointer
```gdb
print *<name>
```
or use constants
```gdb
print *<address>
```

Memory region may be examined using command
```gdb
x <addr>
```

# Crosscompilation

Compiling program with different architecture is called crosscompilation. For this another compiler is needed. In this project `gcc-arm-none-eabi` toolchain will be used. It contains `gcc`, `objdump` and other programs needed for compilation of operations with binaries. String `arm-none-eabi-` is added before each command name. For example, `gcc-arm-none-eabi` compiler is called `arm-none-eabi-gcc`.

# Disassembly

Binary disassembly may be obtained by running rule
```Makefile
make disassemble
```

To get assembly code from C source file, `gcc` may be used. For example, for file `src/foo.c`
```bash
gcc -S src/foo.c
```
will create assembly file `foo.s`.
