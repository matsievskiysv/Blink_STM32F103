#include <foo.h>
#include <stdio.h>

int
main(void)
{
	printf("hello world\n");
	printf("foo(3) = %d; bar(7) = %.3f\n", foo(3), bar(7));
	return 0;
}
