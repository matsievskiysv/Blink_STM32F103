#!/usr/bin/env bash

rm -f compile_commands.json STM32/hal/compile_commands.json
find . -type f -name 'compile_commands.json' -exec jq -n 'inputs' {} + | tee compile_commands.json
cp compile_commands.json STM32/hal/compile_commands.json
