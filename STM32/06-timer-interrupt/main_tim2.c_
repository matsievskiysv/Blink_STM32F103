#include "memmap/gpio.h"
#include "memmap/rcc.h"
#include "memmap/timer.h"
#include "memmap/nvic.h"

volatile unsigned long *const gpioc_odr = (unsigned long *) (GPIO_C + GPIOx_ODR);
volatile unsigned long *const tim2_sr = (unsigned long *) (TIM2 + TIMx_SR);

void
tim2_irqhandler(void)
{
	// clear pending interrupt bit
	*tim2_sr = 0b0000UL;
	// toggle LED
	*gpioc_odr ^= (1 << 13);
}

int
__attribute__ ((noreturn))
main(void)
{
	volatile unsigned long *const rcc_cr = (unsigned long *) (RCC + RCC_CR);
	// enable 8MHz oscillator
	*rcc_cr |= (1 << 0);
	// wait for 8MHz oscillator to stabilize
	while (!(*rcc_cr & (1 << 1))) {}
	// enable GPIOC clock
	volatile unsigned long *const rcc_apb2enr = (unsigned long *) (RCC + RCC_APB2ENR);
	*rcc_apb2enr |= (1 << 4);
	// enable TIM2 clock
	volatile unsigned long *const rcc_apb1enr = (unsigned long *) (RCC + RCC_APB1ENR);
	*rcc_apb1enr |= (1 << 0);
	// enable TIM2 timer interrupt (#28)
	volatile unsigned long *const tim2_dier = (unsigned long *) (TIM2 + TIMx_DIER);
	*tim2_dier |= (1 << 0);
	volatile unsigned long *const nvic_iser_tim2 = (unsigned long *) (NVIC_ISER + NVIC_REG_OFFSET(28));
	*nvic_iser_tim2 |= NVIC_BIT(28);
	// set TIM2 prescaler
	volatile unsigned long *const tim2_psc = (unsigned long *) (TIM2 + TIMx_PSC);
	*tim2_psc			       = 0x3e8UL;
	// set TIM2 auto-reload register
	volatile unsigned long *const tim2_arr = (unsigned long *) (TIM2 + TIMx_ARR);
	*tim2_arr			       = 0x1F40UL;
	// auto-reload TIM2 preload register
	volatile unsigned long *const tim2_cr1 = (unsigned long *) (TIM2 + TIMx_CR1);
	*tim2_cr1 |= (1 << 7);
	// set GPIOC CRH
	volatile unsigned long *const gpioc_crh = (unsigned long *) (GPIO_C + GPIOx_CRH);
	*gpioc_crh				= (*gpioc_crh & ~(0b1111 << 20)) | (0b0010 << 20);
	// set GPIOC output value
	volatile unsigned long *const gpioc_odr = (unsigned long *) (GPIO_C + GPIOx_ODR);
	// disable LED
	*gpioc_odr |= (1 << 13);
	// enable TIM2 timer
	*tim2_cr1 |= (1 << 0);

	for (;;) {}
}
