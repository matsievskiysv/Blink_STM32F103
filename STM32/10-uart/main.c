#include "main.h"

#include "../input_parser/input_parser.h"

#include <dma.h>
#include <gpio.h>
#include <nvic.h>
#include <rcc.h>
#include <signature.h>
#include <stdlib.h>
#include <string.h>
#include <usart.h>
#include <util.h>

enum state {
	PROMPT,
	LISTEN,
	MATCH,
	ACTION,
	MISS,
	STATE_COUNT,
};

volatile bool sending	  = false;
uint8_t	      match_count = 0;

volatile enum state current_state = MISS;
volatile enum state next_state	  = PROMPT;

iprs_state parser_state = {0};

typedef enum {
	IDX_HELP,
	IDX_HELLO,
	IDX_BYE,
	IDX_FLASH_SIZE,
	IDX_CHIP_ID,
	IDX_MATCH_COUNT,
} iprs_string_command_idx_t;

iprs_string commands[] = {
    [IDX_HELP] = {.string = "help\r"},	     [IDX_HELLO] = {.string = "hello\r"},
    [IDX_BYE] = {.string = "bye\r"},	     [IDX_FLASH_SIZE] = {.string = "flash size\r"},
    [IDX_CHIP_ID] = {.string = "chip id\r"}, [IDX_MATCH_COUNT] = {.string = "match count\r"},
};

void prompt(void);
void listen(void);
void match(void);
void action(void);
void miss(void);

void (*const state_transition[STATE_COUNT][STATE_COUNT])(void) = {
    // PROMPT
    {
	// PROMPT
	NULL,
	// LISTEN
	listen,
	// MATCH
	NULL,
	// ACTION
	NULL,
	// MISS
	NULL,
    },
    // LISTEN
    {
	// PROMPT
	NULL,
	// LISTEN
	NULL,
	// MATCH
	match,
	// ACTION
	NULL,
	// MISS
	NULL,
    },
    // MATCH
    {
	// PROMPT
	NULL,
	// LISTEN
	listen,
	// MATCH
	NULL,
	// ACTION
	action,
	// MISS
	miss,
    },
    // ACTION
    {
	// PROMPT
	prompt,
	// LISTEN
	NULL,
	// MATCH
	NULL,
	// ACTION
	NULL,
	// MISS
	NULL,
    },
    // MISS
    {
	// PROMPT
	prompt,
	// LISTEN
	NULL,
	// MATCH
	NULL,
	// ACTION
	NULL,
	// MISS
	NULL,
    },
};

void
change_state(enum state st)
{
	next_state = st;
}

void
transmit_buffer(char *buffer, size_t count)
{
	dma_disable(DMA1, DMA_CH_USART1_TX);
	dma_set_mem_addr(DMA1, DMA_CH_USART1_TX, buffer);
	dma_set_count(DMA1, DMA_CH_USART1_TX, count);
	dma_enable(DMA1, DMA_CH_USART1_TX);
}

void
send_string(char *string)
{
	sending = true;
	transmit_buffer((char *) string, strlen(string));
}

void
function_hello(void)
{
	send_string("\r\nHow do you do!");
}

void
function_help(void)
{
	send_string("\r\nhelp - this message"
		    "\r\nhello - greet"
		    "\r\nbye - goodbye"
		    "\r\nflash size - flash size"
		    "\r\nchip id - unique chip ID"
		    "\r\nmatch count - match counter");
}

void
function_bye(void)
{
	send_string("\r\nHave a nice day!");
}

void
function_flash_size(void)
{
	char string[5] = {0};
	itoa(flash_size(), string, 10);
	send_string("\r\n");
	WAIT_WHILE(sending);
	send_string(string);
	WAIT_WHILE(sending);
	send_string("kB");
}

void
function_chip_id(void)
{
	char	     string[8] = {0};
	unique_id_t *id	       = unique_id();
	send_string("\r\n");
	WAIT_WHILE(sending);
	itoa(id->id[2], string, 16);
	send_string(string);
	itoa(id->id[1], string, 16);
	send_string(string);
	itoa(id->id[0], string, 16);
	send_string(string);
}

void
function_match_count(void)
{
	char string[5] = {0};
	itoa(match_count, string, 10);
	send_string("\r\n");
	WAIT_WHILE(sending);
	send_string(string);
}

void
prompt(void)
{
	send_string(prompt_string);
	iprs_reset(&parser_state);
	change_state(LISTEN);
}

void
listen(void)
{
	dma_disable(DMA1, DMA_CH_USART1_RX);
	dma_set_mem_addr(DMA1, DMA_CH_USART1_RX, data_buffer);
	dma_set_count(DMA1, DMA_CH_USART1_RX, DATA_BUFFER_SIZE);
	dma_enable(DMA1, DMA_CH_USART1_RX);
	enable_interrupt(USART1_INT);
}

void
match(void)
{
	size_t count = DATA_BUFFER_SIZE - dma_get_count(DMA1, DMA_CH_USART1_RX);
	transmit_buffer(data_buffer, count);
	iprs_process(&parser_state, data_buffer, count);
	switch (iprs_status(&parser_state)) {
	case IPRS_CONTINUE:
		change_state(LISTEN);
		break;
	case IPRS_MISS:
		change_state(MISS);
		break;
	case IPRS_MATCH:
		match_count++;
		change_state(ACTION);
		break;
	}
}

void
action(void)
{
	switch ((iprs_string_command_idx_t) iprs_match(&parser_state)) {
	case IDX_HELP:
		function_help();
		break;
	case IDX_HELLO:
		function_hello();
		break;
	case IDX_BYE:
		function_bye();
		break;
	case IDX_FLASH_SIZE:
		function_flash_size();
		break;
	case IDX_CHIP_ID:
		function_chip_id();
		break;
	case IDX_MATCH_COUNT:
		function_match_count();
		break;
	default:
		function_help();
		break;
	}
	change_state(PROMPT);
}

void
miss(void)
{
	function_help();
	change_state(PROMPT);
}

void
usart1_irqhandler(void)
{
	// idle
	// interrupt clear sequence
	usart_status(UART_PORT);
	usart_read(UART_PORT);
	disable_interrupt(USART1_INT);
	dma_disable(DMA1, DMA_CH_USART1_RX);
	change_state(MATCH);
}

void
dma1_channel4_irqhandler(void)
{
	// DMA UART TX
	dma_interrupt_t mask = {.bits = {.global = true, .transfer_complete = true}};
	dma_int_clear(DMA1, DMA_CH_USART1_TX, mask);
	sending = false;
}

int __attribute__((noreturn)) main(void)
{
	memset(data_buffer, 0, sizeof(data_buffer));
	// enable clocks
	{
		rcc_start_internal_clock();
		rcc_clock_t clock = {.apb2 = {.bits = {.iopaen = true, .usart1en = true}},
				     .ahb  = {.bits = {.dma1en = true}}};
		rcc_clock_enable(clock);
	}

	// configure pins for UART
	gpio_configure_output(UART_GPIO_PORT, UART_PIN_TX, GPIO_ALT_PUSH, GPIO_SPEED_LOW);
	gpio_configure_input(UART_GPIO_PORT, UART_PIN_RX, GPIO_IN_PULL, true);

	// configure UART
	usart_configure(UART_PORT, false, false, false, false, USART_STOP_1);
	usart_baudrate(UART_PORT, FREQ, BAUD);

	// enable DMA and interrupts
	{
		usart_status_t usart_interrupt = {.bits = {
						      .idle_detect = true,
						  }};
		usart_configure_interrupt(UART_PORT, usart_interrupt);
		usart_dma(UART_PORT, true, true);
		// enable_interrupt(USART1_INT);
	}

	// configure DMA
	{
		usart_t *uart = GET_REG(UART_PORT);
		dma_config(DMA1, DMA_CH_USART1_RX, data_buffer, (void *) &uart->dr.val, DATA_BUFFER_SIZE, false,
			   DMA_PRIORITY_MEDIUM, DMA_SIZE_8BIT, DMA_SIZE_8BIT, true, false, false, false, false, false,
			   false);
		dma_config(DMA1, DMA_CH_USART1_TX, data_buffer, (void *) &uart->dr.val, DATA_BUFFER_SIZE, false,
			   DMA_PRIORITY_MEDIUM, DMA_SIZE_8BIT, DMA_SIZE_8BIT, true, false, false, true, false, false,
			   true);
		enable_interrupt(DMA1_CH4_INT);
	}

	iprs_init_state(&parser_state, commands, ARRAY_SIZE(commands), -1, 0, 0);

	usart_enable(UART_PORT);

	for (;;) {
		WAIT_WHILE(sending || (current_state == next_state));
		void (*cmd)(void) = state_transition[current_state][next_state];
		current_state	  = next_state;

		if (cmd == NULL)
			continue;

		cmd();
	}
}
