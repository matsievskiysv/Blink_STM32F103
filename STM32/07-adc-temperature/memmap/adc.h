#pragma once

#define ADC1 0x4001##2400##UL
#define ADC2 0x4001##2800##UL
#define ADC3 0x4001##3C00##UL

#define ADCx_SR	   0x00##UL
#define ADCx_CR1   0x04##UL
#define ADCx_CR2   0x08##UL
#define ADCx_SMPR1 0x0C##UL
#define ADCx_SMPR2 0x10##UL
#define ADCx_JOFR1 0x14##UL
#define ADCx_JOFR2 0x18##UL
#define ADCx_JOFR3 0x1C##UL
#define ADCx_JOFR4 0x20##UL
#define ADCx_HTR   0x24##UL
#define ADCx_LTR   0x28##UL
#define ADCx_SQR1  0x2C##UL
#define ADCx_SQR2  0x30##UL
#define ADCx_SQR3  0x34##UL
#define ADCx_JSQR  0x38##UL
#define ADCx_JDR1  0x3C##UL
#define ADCx_JDR2  0x40##UL
#define ADCx_JDR3  0x44##UL
#define ADCx_JDR4  0x48##UL
#define ADCx_DR	   0x4C##UL
