# Temperature sensor
STM32F103 microcontrollers feature an internal temperature sensor connected to the analog-to-digital converter (ADC). In simple words, ADC encodes analog signal in specified voltage range to digital representation by splitting this range in equal segments and finding the closest to the input voltage segment. Number of intervals N relates to ADC resolution M by the equation $N=2^M$.

## Internal reference
STM32F103's ADC has 12 bit resolution. Lowest value `0x000` encodes $V_{ref-}$ voltage, `0xfff` encodes $V_{ref+}$ voltage. In many cases, including STM32F103, $V_{ref-}$ is connected to $V_{SS}$ and $V_{ref+}$ is connected to $V_{DD}$ which are chip's power source potentials. After setting $V_{SS}=0\text{V}$ we still need to know $V_{DD}$ voltage to convert ADV encoded value to voltage. Since external voltage source is not designed to provide a steady reference voltage value, STM32F103 has an internal reference voltage source which provides exactly $V_{ref}=1.2\text{V}$. We may read this voltage by ADC and use it to calculate actual $V_{DD}$ value $$V_{DD}=V_{ref}\frac{V_{DD}^{code}}{V_{ref}^{code}}=1.2\frac{\mathtt{0xfff}}{V_{ref}^{code}}.$$
Any other value from ADC may be converted to volts using formula
$$V_{sens}=V_{DD}\frac{V_{sens}^{code}}{V_{DD}^{code}}
=V_{ref}\frac{V_{DD}^{code}}{V_{ref}^{code}}\frac{V_{sens}^{code}}{V_{DD}^{code}}
=V_{ref}\frac{V_{sens}^{code}}{V_{ref}^{code}}$$

## ADC configuration
In order to read temperature from the sensor we need to
1. start ADC clock;
1. power on temperature sensor and internal reference;
1. set ADC sample time;
1. calibrate ADC;
1. configure ADC conversion sequence;
1. read internal reference value;
1. read temperature value.

This code configures the required registers.

```c
volatile unsigned long *const rcc_cr = (unsigned long *) (RCC + RCC_CR);
// wait for 8MHz oscillator to be ready
while (!(*rcc_cr & (1 << 1))) {}
// enable 8MHz oscillator
*rcc_cr |= (1 << 0);
// enable ADC1 clock
volatile unsigned long *const rcc_apb2enr = (unsigned long *) (RCC + RCC_APB2ENR);
*rcc_apb2enr |= (1 << 9);
// set ADC1 clock prescaler 1/8
volatile unsigned long *const rcc_cfgr = (unsigned long *) (RCC + RCC_CFGR);
*rcc_cfgr |= (0b11 << 14);
// setup ADC1
volatile unsigned long *const adc_sr  = (unsigned long *) (ADC1 + ADCx_SR);
volatile unsigned long *const adc_cr1 = (unsigned long *) (ADC1 + ADCx_CR1);
*adc_cr1 |= (1 << 11); // DISCEN
volatile unsigned long *const adc_cr2 = (unsigned long *) (ADC1 + ADCx_CR2);
*adc_cr2 |= (1 << 23)   // TSVREFE
		| (1 << 0); // ADON
// setup ADC1 maximum sample time
volatile unsigned long *const adc_smpr1 = (unsigned long *) (ADC1 + ADCx_SMPR1);
*adc_smpr1 |= (0b111 << 21) | (0b111 << 18);
// setup ADC1 sequence
volatile unsigned long *const adc_sqr3 = (unsigned long *) (ADC1 + ADCx_SQR3);
*adc_sqr3                  = (16 << 0);
// ADC1 data
volatile unsigned long *const adc_dr = (unsigned long *) (ADC1 + ADCx_DR);

// Start calibration
*adc_cr2 |= (1 << 2);
while (*adc_cr2 & (1 << 2)) {} // wait until complete

for (;;) {
	unsigned int vref = 0, vsens = 0;
	// select internal reference
	*adc_sqr3 = (17 << 0);
	// Start conversion
	*adc_cr2 |= (1 << 0);
	while (!(*adc_sr & (1 << 1))) {} // wait until complete
	vref    = (*adc_dr & 0xfff);
	*adc_sr = 0; // reset status register

	// select sensor
	*adc_sqr3 = (16 << 0);
	// Start conversion
	*adc_cr2 |= (1 << 0);
	while (!(*adc_sr & (1 << 1))) {} // wait until complete
	vsens   = (*adc_dr & 0xfff);
	*adc_sr = 0; // reset status register

    ...
}
```

## Fixed point evaluation
Lines omitted in previous code snipped (replaced by `...`) contains the actual temperature calculation code.
From the datasheet we obtain the equation for temperature in degrees of Celsius and typical constant values.
$$T^\circ\text{C} = \frac{V_{25} - V_{sens}}{Sl_{Avg}} + 25$$
$$V_{25} = 1.43\text{V}$$
$$Sl_{Avg} = 4.3\frac{\text{mV}}{{}^\circ\text{C}}=0.0043\frac{\text{V}}{{}^\circ\text{C}}$$
Combining it with previously derived formula for $V_{sens}$ we get
$$T = \frac{V_{25} - V_{ref}\frac{V_{sens}^{code}}{V_{ref}^{code}}}{Sl_{Avg}} + 25$$
$$T = \frac{1.43 - 1.2\frac{V_{sens}^{code}}{V_{ref}^{code}}}{0.0043} + 25$$
We cannot use this equation right away because STM32F103 does not have FPU unit and can operate only with the whole numbers. To overcome this firstly we convert degrees of Celsius to milli-Celsius.
$$T = \frac{1430 - 1200\frac{V_{sens}^{code}}{V_{ref}^{code}}}{0.0043} + 25000$$
Now we only have whole numbers in numerator but still there's a real number in denominator. We will get rid of it by multiplying both numerator and denominator by $10^4$.
$$T = \frac{14300000 - 12000000\frac{V_{sens}^{code}}{V_{ref}^{code}}}{43} + 25000$$
While this equation is technically correct, it may suffer from variable overflow because result of $12000000 V_{sens}^{code}$ may get quite big. Solution to this is either to use bigger variables or to slightly modify equation.
$$T = \frac{1000\left(14300 - 12000\frac{V_{sens}^{code}}{V_{ref}^{code}}\right)}{43} + 25000$$
C code for this equation is
```c
temp = (14300 - (12000 * vsens) / vref) * 1000 / 43 + 25000;
```

## Soft FPU
Even if microprocessor lacks FPU unit it's still possible to use floating point numbers in C by emulating FPU instructions using regular registers. To instruct compiler to use these we need to add `-mfloat-abi=soft` compilation argument.
Using soft FPU our code will look like this
```c
temp = (1.43 - (1.2 * vsens) / vref) / 0.0043 * 1000 + 25000;
```
While this looks more attractive, soft FPU is quite expansive. Firstly, soft FPU is slow even compared to hard FPU. Secondly, it takes more space. Compilation of this project with and without soft FPU produces 3404b and 700b binaries.

## Select build type
It's usual for C programs to have a build time configurations. It is done with preprocessor directives.
In our program `temp` evaluation expression is selected based on the `__FPU_PRESENT` preprocessor definition
```c
#ifdef __FPU_PRESENT
		temp = (1.43 - (1.2 * vsens) / vref) / 0.0043 * 1000 + 25000;
#else
		temp = (14300 - (12000 * vsens) / vref) * 1000 / 43 + 25000;
#endif
```
For smaller programs these configurations are defined using compiler `-D` flag. For example, `gcc -D__FPU_PRESENT -o main main.c`. In case of bigger projects compiled using compilation frameworks like [autotools](https://www.gnu.org/software/automake/manual/html_node/Autotools-Introduction.html) or [cmake](https://cmake.org/) these definitions are usually supplied by auto-generated `config.h` header.

# References

https://en.wikipedia.org/wiki/Analog-to-digital_converter
