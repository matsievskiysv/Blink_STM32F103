#include "memmap/adc.h"
#include "memmap/rcc.h"

int temp = 0;

int __attribute__((noreturn)) main(void)
{
	volatile unsigned long *const rcc_cr = (unsigned long *) (RCC + RCC_CR);
	// enable 8MHz oscillator
	*rcc_cr |= (1 << 0);
	// wait for 8MHz oscillator to stabilize
	while (!(*rcc_cr & (1 << 1))) {}
	// enable ADC1 clock
	volatile unsigned long *const rcc_apb2enr = (unsigned long *) (RCC + RCC_APB2ENR);
	*rcc_apb2enr |= (1 << 9);
	// set ADC1 clock prescaler 1/8
	volatile unsigned long *const rcc_cfgr = (unsigned long *) (RCC + RCC_CFGR);
	*rcc_cfgr |= (0b11 << 14);
	// setup ADC1
	volatile unsigned long *const adc_sr  = (unsigned long *) (ADC1 + ADCx_SR);
	volatile unsigned long *const adc_cr1 = (unsigned long *) (ADC1 + ADCx_CR1);
	*adc_cr1 |= (1 << 11); // DISCEN
	volatile unsigned long *const adc_cr2 = (unsigned long *) (ADC1 + ADCx_CR2);
	*adc_cr2 |= (1 << 23)	// TSVREFE
		    | (1 << 0); // ADON
	// setup ADC1 maximum sample time
	volatile unsigned long *const adc_smpr1 = (unsigned long *) (ADC1 + ADCx_SMPR1);
	*adc_smpr1 |= (0b111 << 21) | (0b111 << 18);
	// setup ADC1 sequence
	volatile unsigned long *const adc_sqr3 = (unsigned long *) (ADC1 + ADCx_SQR3);
	*adc_sqr3			       = (16 << 0);
	// ADC1 data
	volatile unsigned long *const adc_dr = (unsigned long *) (ADC1 + ADCx_DR);

	// Start calibration
	*adc_cr2 |= (1 << 2);
	while (*adc_cr2 & (1 << 2)) {} // wait until complete

	for (;;) {
		unsigned int vref = 0, vsens = 0;
		// select internal reference
		*adc_sqr3 = (17 << 0);
		// Start conversion
		*adc_cr2 |= (1 << 0);
		while (!(*adc_sr & (1 << 1))) {} // wait until complete
		vref	= (*adc_dr & 0xfff);
		*adc_sr = 0; // reset status register

		// select sensor
		*adc_sqr3 = (16 << 0);
		// Start conversion
		*adc_cr2 |= (1 << 0);
		while (!(*adc_sr & (1 << 1))) {} // wait until complete
		vsens	= (*adc_dr & 0xfff);
		*adc_sr = 0; // reset status register

#ifdef __FPU_PRESENT
		temp = (1.43 - (1.2 * vsens) / vref) / 0.0043 * 1000 + 25000;
#else
		temp = (14300 - (12000 * vsens) / vref) * 1000 / 43 + 25000;
#endif
	}
}
