This repository contains a bare-metal [ARM](https://en.wikipedia.org/wiki/ARM_architecture) project for [STM32F103](https://www.st.com/en/microcontrollers-microprocessors/stm32f103.html) chip. It is build with [GNU](https://www.gnu.org/) tool chain whit use of [OpenOCD](http://openocd.org/) debugger (STLink interface) and is kept is simple is possible.

> STM32F103 boot process is controlled via boot pins (usually wired to jumpers). Be sure to set Boot mode jumpers to `BOOT0=1` and `BOOT1=1` to load from RAM and `BOOT0=0` and `BOOT1=0` to load from Flash.
{.is-warning}

1. [openocd](./00-openocd) - configuration of OpenOCD debugger
1. [ram assembly](./01-ram-assembly) - run assembly code from RAM
1. [flash-assembly](./02-flash-assembly) - run assembly code from Flash
1. [flash-assembly-relocaiton](./03-flash-assembly-relocaiton) - run assembly code from Flash with relocation to RAM
1. [flash-c](./04-flash-c) - run C code from Flash
1. [stack-overflow](./05-stack-overflow) - stack overflow example
1. [timer-interrupt](./06-timer-interrupt) - use hardware timer for delay
1. [adc-temperature](./07-adc-temperature) - read internal temperature sensor connected to ADC

[`gdbinit`](./gdbinit) and [`openocd.cfg`](./openocd.cfg) are `gdb` and `openocd` configuration
files and are shared between projects.
