#include "io.h"

void
sleep_cycle(unsigned int cycles)
{
	for (unsigned int i = 0; i < cycles; i++) {}
}

int __attribute__((noreturn)) main(void)
{
	// enable GPIOC clock
	volatile unsigned long *const rcc_ahb2enr = (unsigned long *) (RCC_BASE + RCC_AHB2ENR);
	*rcc_ahb2enr |= (1 << 4);
	// set GPIOC CRH
	volatile unsigned long *const gpioc_crh = (unsigned long *) (GPIO_C + GPIOx_CRH);
	*gpioc_crh				= (*gpioc_crh & ~(0b1111 << 20)) | (0b0010 << 20);
	// set GPIOC output value
	volatile unsigned long *const gpioc_odr = (unsigned long *) (GPIO_C + GPIOx_ODR);
	*gpioc_odr |= (1 << 13);

	for (;;) {
		// toggle GPIOC output value
		*gpioc_odr ^= (1 << 13);
		sleep_cycle(0x80000);
	}
}
