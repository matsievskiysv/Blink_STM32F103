# OpenOCD debugging

In this series we will use [OpenOCD](https://openocd.org/) to communicate with STM32 microcontroller: to upload firmware and debug. Therefore, firstly we need to configure OpenOCD.

## JTAG bus

IEEE 1149.1 or JTAG is a serial bus that is used for PCB in place tests and debug. For more information about it take a look at [JTAG and boundary scan tutorial by Ben Bennetts](https://fiona.dmcs.pl/~rkielbik/nid/IEEE_1149_JTAG_and_Boundary_Scan_Tutorial.pdf).

JTAG bus uses four signal pins: `TCK`, `TDI`, `TDO` and `TMS`. But in many cases `TRST` and `SRST` (chip reset) are also required to be connected. Connecting chip and adapter `GND` is required. Therefore, the wiring table is as follows:

| Chip | Adapter |
|:--|:--|
| `Vcc` | `Vcc` |
| `GND` | `GND` |
| `TCK` | `TCK` |
| `TDI` | `TDI` |
| `TDO` | `TDO` |
| `TMS` | `TMS` |
| `TRST` | `TRST` |
| `SRST` | `SRST` |

> If you power your chip externally, do not connect `Vcc` of the chip and adapter if you don't want to let the [magic smoke](https://en.wikipedia.org/wiki/Magic_smoke) out.
{.is-warning}

## OpenOCD configuration

### Adapter

We will use FTDI [FT2232HL](https://ftdichip.com/products/ft2232hl/) based JTAG adapter at first. In the end we will switch to cheap [STLINKv2](https://www.st.com/en/development-tools/st-link-v2.html) knockoff.

After connecting JTAG adapter wires we need to find its OpenOCD's driver name. For this issue the command

```bash
openocd -c 'adapter list'
```

In this case adapter name is `ftdi`. Note, that OpenOCD is throwing error hinting what configuration is missing

```
Error: Debug Adapter has to be specified, see "adapter driver" command
```

As the first step we created configuration file [`jtag-01.cfg`](./jtag-01.cfg) with the line

```tcl
adapter driver ftdi
```

We ran it with command

```bash
openocd -f jtag-01.cfg
```

and immediately hit another error and a warning

```
Warn : An adapter speed is not selected in the init scripts. OpenOCD will try to run the adapter at the low speed (100 kHz)
Warn : To remove this warnings and achieve reasonable communication speed with the target, set "adapter speed" or "jtag_rclk" in the init scripts.
Error: Please specify ftdi vid_pid
```

JTAG bus speed is
$f_{JTAG}\approx\frac{f_{chip}}{6}$. STM32 blue pill comes with 8 MHz oscillator, so we will set bus speed to 1 MHz just to be sure. `PID` and `VID` can be easily found by issuing `lsusb` command.

If after specifying `PID` and `VID` you still get access errors, you might need permissions to access the FTDI board. Either prepend `openocd` command with `sudo` or add

```
ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6010", ATTRS{product}=="Dual RS232-HS", MODE="0660", GROUP="plugdev"
```

to `/etc/udev/rules.d/99-ftdi.rules`.

Our second attempt [`jtag-02.cfg`](./jtag-02.cfg) looks like this

```tcl
adapter driver ftdi
adapter speed 1000
ftdi vid_pid 0x0403 0x6010
transport select jtag
```

It throws rather cryptic error

```
Error: JTAG scan chain interrogation failed: all ones
Error: Check JTAG interface, timings, target power, etc.
Error: Trying to use configured scan chain anyway...
```

The problem is that we didn't configure our adapter yet. We consult [OpenOCD's user manual](https://openocd.org/doc-release/html/Debug-Adapter-Configuration.html) to learn our adapter commands.

The first command we need to consider is `ftdi channel`. Our adapter has two channels, and we need to tell OpenOCD which one we are using. In this case adapter is connected to the channel `1`.

The next command is `ftdi layout_init`. It configures each of 16 pins of the FTDI channel using bitmasks. We need to configure outputs according to the table

| Pin | Direction |
|:--|:--|
| `TCK` | Output |
| `TDI` | Output |
| `TDO` | Input |
| `TMS` | Output |
| `TRST` | Output |
| `SRST` | Output |

`TDO` is the only channel that receives data, therefore it is configured as input. The other channels are configured as outputs. Unused pins will be configured as inputs. `ftdi layout_init` command interprets `1` as output and `0` as input. For the output channel `1` is interpreted as high voltage level and `0` as low level.

| | 15 | 14 | 13 | 12 | 11 | 10 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 | HEX |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
| Direction | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 | 1 | 1 | 0 | 1 | 1 | 0x3b |
| Value | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 | 1 | 0 | 0 | 0 | 0 | 0x30 |

> Here we assume that `TRST` and `SRST` signals are connected to `GPIO0` and `GPIO1` pins of the FTDI adapter.
{.is-info}

Then, we need to explicitly tell OpenOCD which adapter pins are `TRST` and `SRST` and how to access them. `TRST` mask bit is `1<<4` and `TRST` mask bit is `1<<5`. We pass this information to the OpenOCD via commands

```tcl
ftdi layout_signal nTRST -data 0x10 -input 0x10
ftdi layout_signal nSRST -data 0x20 -input 0x20 -oe 0x20
```

add some delay for these signals

```tcl
adapter srst delay 100
jtag_ntrst_delay 100
```

and then instruct it to use both

```tcl
reset_config trst_and_srst
```

Now, our configuration [`jtag-03.cfg`](./jtag-03.cfg) is as follows

```tcl
adapter driver ftdi
adapter speed 1000
ftdi vid_pid 0x0403 0x6010
transport select jtag
ftdi channel 1
ftdi layout_init 0x30 0x3b
ftdi layout_signal nTRST -data 0x10 -input 0x10
ftdi layout_signal nSRST -data 0x20 -input 0x20 -oe 0x20
adapter srst delay 100
jtag_ntrst_delay 100
reset_config trst_and_srst
init
puts [scan_chain]
```

and it should print a nice table like this

```
   TapName             Enabled  IdCode     Expected   IrLen IrCap IrMask
-- ------------------- -------- ---------- ---------- ----- ----- ------
 0 auto0.tap              Y     0x3ba00477 0x00000000     4 0x01  0x03
 1 auto1.tap              Y     0x16410041 0x00000000     5 0x01  0x03
```

### Target configuration

Rows in table above are chip's JTAG TAPs (Test Access Point). We copy-paste these values to our configuration file.

```tcl
jtag newtap stm cpu -irlen 4 -ircapture 0x1 -irmask 0x03 -expected-id 0x3ba00477
jtag newtap stm bs -irlen 5 -ircapture 0x1 -irmask 0x03 -expected-id 0x16410041
```

We know that these TAPs are of type `cpu` and `bs` from the `STM32` datasheet. ARM architecture requires setting up DAP (Debug Access Point) for each CPU.

The updated version of the configuration [`jtag-04.cfg`](./jtag-04.cfg) is as follows

```tcl
adapter driver ftdi
adapter speed 1000
ftdi vid_pid 0x0403 0x6010
transport select jtag
ftdi channel 1
ftdi layout_init 0x30 0x3b
ftdi layout_signal nTRST -data 0x10 -input 0x10
ftdi layout_signal nSRST -data 0x20 -input 0x20 -oe 0x20
reset_config trst_and_srst
jtag newtap stm cpu -irlen 4 -ircapture 0x1 -irmask 0x03 -expected-id 0x3ba00477
jtag newtap stm bs -irlen 5 -ircapture 0x1 -irmask 0x03 -expected-id 0x16410041
dap create stm.dap -chain-position stm.cpu
target create stm.tgt cortex_m -endian little -dap stm.dap
init
puts [scan_chain]
puts [targets]
```

At this point we can connect to debugger via TelNet and control CPU

```bash
telnet 127.0.0.1 4444
```

Typing commands

```tcl
reset halt
reg
```

will reset board to initial conditions and display contents of CPU registers.

### Flash configuration

All that's left is to configure board's Flash, so we could upload firmware directly via JTAG adapter.

Fortunately, OpenOCD has needed driver `stm32f1x` for our chip, which will query all the needed information. The only value we need to provide is the base address of the flash memory, which could be found from the chip's memory map, which is described in the datasheet.

```tcl
flash bank stm.flash stm32f1x 0x08000000 0 0 0 stm.tgt
```

In order to speed up firmware upload we configure a work area for the chip. This is a place in RAM where OpenOCD will cache data.

```tcl
stm.tgt configure -work-area-phys 0x20000000 -work-area-size 0x1000 -work-area-backup 0
```

The script now looks like this

```tcl
adapter driver ftdi
adapter speed 1000
ftdi vid_pid 0x0403 0x6010
transport select jtag
ftdi channel 1
ftdi layout_init 0x30 0x3b
ftdi layout_signal nTRST -data 0x10 -input 0x10
ftdi layout_signal nSRST -data 0x20 -input 0x20 -oe 0x20
adapter srst delay 100
jtag_ntrst_delay 100
reset_config trst_and_srst
jtag newtap stm cpu -irlen 4 -ircapture 0x1 -irmask 0x03 -expected-id 0x3ba00477
jtag newtap stm bs -irlen 5 -ircapture 0x1 -irmask 0x03 -expected-id 0x16410041
dap create stm.dap -chain-position stm.cpu
target create stm.tgt cortex_m -endian little -dap stm.dap
stm.tgt configure -work-area-phys 0x20000000 -work-area-size 0x1000 -work-area-backup 0
flash bank stm.flash stm32f1x 0x08000000 0 0 0 stm.tgt
init
puts [scan_chain]
puts [targets]
```

### GDB configuration

Some minor tweaks are needed to make debugging with GDB more pleasant.

Firstly, OpenOCD has two ways of communication with GDB: via socket or via pipes. If we want to use pipes, we need to add the following command to our configuration

```tcl
gdb_port pipe
```

and to redirect OpenOCD's log to a file

```tcl
log_output openocd.log
```

OpenOCD allows adding callbacks to certain events, such as GDB attach/detouch events

```tcl
stm.cpu configure -event gdb-attach { halt }
stm.cpu configure -event gdb-detach { resume }
```

These commands are not needed when using pipes since GDB attach event happens at the same time as OpenOCD starts, but are handy when using socket connection.

Any OpenOCD command may be added after the `init` command. For example, we may wish to reset and halt our board when starting OpenOCD. For this we simply add

```tcl
reset halt
```

after the `init` command.

Our final script is as follows

```tcl
adapter driver ftdi
adapter speed 1000
ftdi vid_pid 0x0403 0x6010
transport select jtag
ftdi channel 1
ftdi layout_init 0x30 0x3b
ftdi layout_signal nTRST -data 0x10 -input 0x10
ftdi layout_signal nSRST -data 0x20 -input 0x20 -oe 0x20
adapter srst delay 100
jtag_ntrst_delay 100
reset_config trst_and_srst
jtag newtap stm cpu -irlen 4 -ircapture 0x1 -irmask 0x03 -expected-id 0x3ba00477
jtag newtap stm bs -irlen 5 -ircapture 0x1 -irmask 0x03 -expected-id 0x16410041
dap create stm.dap -chain-position stm.cpu
target create stm.tgt cortex_m -endian little -dap stm.dap
stm.tgt configure -work-area-phys 0x20000000 -work-area-size 0x1000 -work-area-backup 0
flash bank stm.flash stm32f1x 0x08000000 0 0 0 stm.tgt
gdb_port pipe
log_output openocd.log
stm.cpu configure -event gdb-attach { halt }
stm.cpu configure -event gdb-detach { resume }
init
reset halt
```

Finally, we may add aliases to the frequently used commands in GDB. For example, this macro erases chip's flash, loads the new firmware and then resets and halts the chip

```gdb
define mflash_load
  monitor flash write_image erase unlock $arg0
  monitor flash verify_image $arg0
  monitor reset halt
end
```

These commands are placed in [`gdbinit`](../gdbinit) file

Now, GDB and OpenOCD may be started together in one line

```bash
gdb-multiarch -ix gdbinit -ex 'target extended-remote | openocd -f jtag-06.cfg'
```

## Using existing configuration

Now, when we learned how to create OpenOCD configuration file for JTAG adapter, we will learn how to use preexisting configurations for STLinkv2 single wire adapter.

OpenOCD comes with many configuration scripts. We will assume that OpenOCD is installed to `/opt/openocd` directory.

The first thing you want to do is to replace created earlier file `/etc/udev/rules.d/99-ftdi.rules` with bundled `/opt/openocd/share/openocd/contrib/60-openocd.rules` which sets up permissions for many adapters.

OpenOCD is configured to search for configuration files in installation directory `/opt/openocd/share/openocd/scripts`.

Firstly, we search for adapter configuration and apply it with command

```tcl
source [find "interface/stlink.cfg"]
```

Since our adapter supports multiple transports, we explicitly select one

```tcl
transport select hla_swd
```

Secondly, we search for our board or target. If your board is supported, search for it in `board` folder

```tcl
source [find "board/stm32f103c8_blue_pill.cfg"]
```

It not, search for the CPU target

```tcl
source [find "target/stm32f1x.cfg"]
```

In our case these commands are equivalent, but if your board is more complex, using board scripts is preferable.

Thus, our configuration script [`swd.cfg`](./swd.cfg) is as follows

```tcl
source [find "interface/stlink.cfg"]
transport select hla_swd
source [find "target/stm32f1x.cfg"]
gdb_port pipe
log_output openocd.log
init
reset halt
```

It is significantly simpler, community tested and resistant to OpenOCD's API changes.
