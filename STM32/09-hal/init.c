#include <string.h>

extern size_t _data_vma, _data_evma, _data_lma; // symbols

int main(void) __attribute__((noreturn));

void
reset_handler(void)
{
	// relocate data section
	memcpy(&_data_vma, &_data_lma, (&_data_evma - &_data_vma) * sizeof(size_t));
	main();
}
