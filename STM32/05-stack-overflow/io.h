#pragma once

// Memory region base addresses
#define FLASH_BASE    0x0800##0000##UL
#define RAM_BASE      0x2000##0000##UL
#define SYSTEM_MEMORY 0x1fff##f000##UL
#define PERIPHERALS   0x4000##0000##UL

// GPIO
#define GPIO_A	   0x4001##0800##UL
#define GPIO_B	   0x4001##0c00##UL
#define GPIO_C	   0x4001##1000##UL
#define GPIO_D	   0x4001##1400##UL
#define GPIO_E	   0x4001##1800##UL
#define GPIO_F	   0x4001##1C00##UL
#define GPIO_G	   0x4001##2000##UL
#define GPIOx_CRL  0x00##UL
#define GPIOx_CRH  0x04##UL
#define GPIOx_IDR  0x08##UL
#define GPIOx_ODR  0x0c##UL
#define GPIOx_BSSR 0x10##UL
#define GPIOx_BRR  0x14##UL
#define GPIOx_LCKR 0x18##UL

// RCC
#define RCC_BASE     0x4002##1000##UL
#define RCC_CR	     0x00##UL
#define RCC_CFGR     0x04##UL
#define RCC_CIR	     0x08##UL
#define RCC_APB2RSTR 0x0c##UL
#define RCC_APB1RSTR 0x10##UL
#define RCC_AHBENR   0x14##UL
#define RCC_AHB2ENR  0x18##UL
#define RCC_APB1ENR  0x1c##UL
#define RCC_BDCR     0x20##UL
#define RCC_CSR	     0x24##UL
#define RCC_AHBRSTR  0x28##UL
#define RCC_CFGR2    0x2c##UL
