#include <usb_cdc.h>
#include <util.h>

usb_btable_entry_t SHARED_SRAM_REGION usb_btable[5];
size_t SHARED_SRAM_REGION	      usb_buffer_ep0_rx[SHARED_SRAM_ARRAY_SIZE(USB_EP0_SIZE, size_t)];
size_t SHARED_SRAM_REGION	      usb_buffer_ep0_tx[SHARED_SRAM_ARRAY_SIZE(USB_EP0_SIZE, size_t)];
size_t SHARED_SRAM_REGION	      usb_buffer_cdc_bulk_data_rx[SHARED_SRAM_ARRAY_SIZE(USB_EP0_SIZE, size_t)];
size_t SHARED_SRAM_REGION	      usb_buffer_cdc_bulk_data_tx[SHARED_SRAM_ARRAY_SIZE(USB_EP0_SIZE, size_t)];
size_t SHARED_SRAM_REGION	      usb_buffer_cdc_bulk_control_rx[SHARED_SRAM_ARRAY_SIZE(USB_EP0_SIZE, size_t)];
size_t SHARED_SRAM_REGION	      usb_buffer_cdc_bulk_control_tx[SHARED_SRAM_ARRAY_SIZE(USB_EP0_SIZE, size_t)];
size_t SHARED_SRAM_REGION	      usb_buffer_cdc_itr_tx[SHARED_SRAM_ARRAY_SIZE(sizeof(usb_setup_packet_t), size_t)];

typedef enum {
	USB_DESCRIPTOR_STRING_IDX_MFR,
	USB_DESCRIPTOR_STRING_IDX_PRODUCT,
	USB_DESCRIPTOR_STRING_IDX_SERIALNUMBER,
	USB_DESCRIPTOR_STRING_IDX_INTERFACE_CONTROL,
	USB_DESCRIPTOR_STRING_IDX_INTERFACE_DATA,
} usb_descriptor_string_idx_t;

const usb_descriptor_device_t device_descriptor = {
    .bLength		= sizeof(usb_descriptor_device_t),
    .bDescriptorType	= USB_DESCRIPTOR_DEVICE,
    .bcdUSB		= USB_REVISION,
    .bDeviceClass	= USB_CDC_DEVICE_CLASS,
    .bDeviceSubClass	= USB_CDC_DEVICE_SUBCLASS,
    .bDeviceProtocol	= USB_CDC_DEVICE_PROTOCOL,
    .bMaxPacketSize0	= USB_EP0_SIZE,
    .idVendor		= USB_CDC_ID_VENDOR,
    .idProduct		= USB_CDC_ID_PRODUCT,
    .bcdDevice		= 0x0001,
    .iManufacturer	= USB_STRING_IDX_TO_NUM(USB_DESCRIPTOR_STRING_IDX_MFR),
    .iProduct		= USB_STRING_IDX_TO_NUM(USB_DESCRIPTOR_STRING_IDX_PRODUCT),
    .iSerialNumber	= USB_STRING_IDX_TO_NUM(USB_DESCRIPTOR_STRING_IDX_SERIALNUMBER),
    .bNumConfigurations = 1};

const usb_descriptor_endpoint_t usb_descriptor_endpoint_cdc_data_data[] = {
    {.bLength		  = sizeof(usb_descriptor_endpoint_t),
     .bDescriptorType	  = USB_DESCRIPTOR_ENDPOINT,
     .EndpointNumber	  = 0x02,
     .Direction		  = USB_ENDPOINT_DIRECTION_IN,
     .TransferType	  = USB_TRANSFER_TYPE_BULK,
     .SynchronizationType = USB_SYNCHRONIZATION_TYPE_NO_SYNCHRONIZATION,
     .UsageType		  = USB_USAGE_TYPE_DATA_ENDPOINT,
     .wMaxPacketSize	  = 64,
     .bInterval		  = 0xFF},
    {.bLength		  = sizeof(usb_descriptor_endpoint_t),
     .bDescriptorType	  = USB_DESCRIPTOR_ENDPOINT,
     .EndpointNumber	  = 0x02,
     .Direction		  = USB_ENDPOINT_DIRECTION_OUT,
     .TransferType	  = USB_TRANSFER_TYPE_BULK,
     .SynchronizationType = USB_SYNCHRONIZATION_TYPE_NO_SYNCHRONIZATION,
     .UsageType		  = USB_USAGE_TYPE_DATA_ENDPOINT,
     .wMaxPacketSize	  = 64,
     .bInterval		  = 0xFF}};

const usb_descriptor_endpoint_t usb_descriptor_endpoint_cdc_control_data[] = {
    {.bLength		  = sizeof(usb_descriptor_endpoint_t),
     .bDescriptorType	  = USB_DESCRIPTOR_ENDPOINT,
     .EndpointNumber	  = 0x04,
     .Direction		  = USB_ENDPOINT_DIRECTION_IN,
     .TransferType	  = USB_TRANSFER_TYPE_BULK,
     .SynchronizationType = USB_SYNCHRONIZATION_TYPE_NO_SYNCHRONIZATION,
     .UsageType		  = USB_USAGE_TYPE_DATA_ENDPOINT,
     .wMaxPacketSize	  = 64,
     .bInterval		  = 0xFF},
    {.bLength		  = sizeof(usb_descriptor_endpoint_t),
     .bDescriptorType	  = USB_DESCRIPTOR_ENDPOINT,
     .EndpointNumber	  = 0x04,
     .Direction		  = USB_ENDPOINT_DIRECTION_OUT,
     .TransferType	  = USB_TRANSFER_TYPE_BULK,
     .SynchronizationType = USB_SYNCHRONIZATION_TYPE_NO_SYNCHRONIZATION,
     .UsageType		  = USB_USAGE_TYPE_DATA_ENDPOINT,
     .wMaxPacketSize	  = 64,
     .bInterval		  = 0xFF}};

const usb_descriptor_endpoint_t usb_descriptor_endpoint_cdc_data_mgmt[] = {
    {.bLength		  = sizeof(usb_descriptor_endpoint_t),
     .bDescriptorType	  = USB_DESCRIPTOR_ENDPOINT,
     .EndpointNumber	  = 0x01,
     .Direction		  = USB_ENDPOINT_DIRECTION_IN,
     .TransferType	  = USB_TRANSFER_TYPE_INTERRUPT,
     .SynchronizationType = USB_SYNCHRONIZATION_TYPE_NO_SYNCHRONIZATION,
     .UsageType		  = USB_USAGE_TYPE_DATA_ENDPOINT,
     .wMaxPacketSize	  = 8,
     .bInterval		  = 0xFF}};

const usb_descriptor_endpoint_t usb_descriptor_endpoint_cdc_control_mgmt[] = {
    {.bLength		  = sizeof(usb_descriptor_endpoint_t),
     .bDescriptorType	  = USB_DESCRIPTOR_ENDPOINT,
     .EndpointNumber	  = 0x03,
     .Direction		  = USB_ENDPOINT_DIRECTION_IN,
     .TransferType	  = USB_TRANSFER_TYPE_INTERRUPT,
     .SynchronizationType = USB_SYNCHRONIZATION_TYPE_NO_SYNCHRONIZATION,
     .UsageType		  = USB_USAGE_TYPE_DATA_ENDPOINT,
     .wMaxPacketSize	  = 8,
     .bInterval		  = 0xFF}};

const usb_descriptor_interface_t usb_descriptor_interface_cdc[] = {
    {.bLength		 = sizeof(usb_descriptor_interface_t),
     .bDescriptorType	 = USB_DESCRIPTOR_INTERFACE,
     .bInterfaceNumber	 = 0,
     .bAlternateSetting	 = 0,
     .bNumEndpoints	 = ARRAY_SIZE(usb_descriptor_endpoint_cdc_data_mgmt),
     .bInterfaceClass	 = USB_CDC_INTERFACE_MGMT_CLASS,
     .bInterfaceSubClass = USB_CDC_INTERFACE_MGMT_SUBCLASS,
     .bInterfaceProtocol = USB_CDC_INTERFACE_MGMT_PROTOCOL,
     .iInterface	 = USB_STRING_IDX_TO_NUM(USB_DESCRIPTOR_STRING_IDX_INTERFACE_DATA)},
    {.bLength		 = sizeof(usb_descriptor_interface_t),
     .bDescriptorType	 = USB_DESCRIPTOR_INTERFACE,
     .bInterfaceNumber	 = 1,
     .bAlternateSetting	 = 0,
     .bNumEndpoints	 = ARRAY_SIZE(usb_descriptor_endpoint_cdc_data_data),
     .bInterfaceClass	 = USB_CDC_INTERFACE_DATA_CLASS,
     .bInterfaceSubClass = USB_CDC_INTERFACE_DATA_SUBCLASS,
     .bInterfaceProtocol = USB_CDC_INTERFACE_DATA_PROTOCOL,
     .iInterface	 = USB_STRING_IDX_TO_NUM(USB_DESCRIPTOR_STRING_IDX_INTERFACE_DATA)},
    {.bLength		 = sizeof(usb_descriptor_interface_t),
     .bDescriptorType	 = USB_DESCRIPTOR_INTERFACE,
     .bInterfaceNumber	 = 2,
     .bAlternateSetting	 = 0,
     .bNumEndpoints	 = ARRAY_SIZE(usb_descriptor_endpoint_cdc_data_mgmt),
     .bInterfaceClass	 = USB_CDC_INTERFACE_MGMT_CLASS,
     .bInterfaceSubClass = USB_CDC_INTERFACE_MGMT_SUBCLASS,
     .bInterfaceProtocol = USB_CDC_INTERFACE_MGMT_PROTOCOL,
     .iInterface	 = USB_STRING_IDX_TO_NUM(USB_DESCRIPTOR_STRING_IDX_INTERFACE_CONTROL)},
    {.bLength		 = sizeof(usb_descriptor_interface_t),
     .bDescriptorType	 = USB_DESCRIPTOR_INTERFACE,
     .bInterfaceNumber	 = 3,
     .bAlternateSetting	 = 0,
     .bNumEndpoints	 = ARRAY_SIZE(usb_descriptor_endpoint_cdc_data_data),
     .bInterfaceClass	 = USB_CDC_INTERFACE_DATA_CLASS,
     .bInterfaceSubClass = USB_CDC_INTERFACE_DATA_SUBCLASS,
     .bInterfaceProtocol = USB_CDC_INTERFACE_DATA_PROTOCOL,
     .iInterface	 = USB_STRING_IDX_TO_NUM(USB_DESCRIPTOR_STRING_IDX_INTERFACE_CONTROL)}};

const usb_cdc_descriptor_header_t usb_cdc_descriptor_header = {
    .bFunctionLength	= sizeof(usb_cdc_descriptor_header_t),
    .bDescriptorType	= USB_CDC_DESCRIPTOR_TYPE_CS_INTERFACE,
    .bDescriptorSubtype = USB_CDC_DESCRIPTOR_SUBTYPE_HEADER,
    .bcdCDC		= USB_CDC_REVISION,
};

const usb_cdc_descriptor_union_t usb_cdc_descriptor_union_data = {
    .bFunctionLength	   = sizeof(usb_cdc_descriptor_union_t),
    .bDescriptorType	   = USB_CDC_DESCRIPTOR_TYPE_CS_INTERFACE,
    .bDescriptorSubtype	   = USB_CDC_DESCRIPTOR_SUBTYPE_UNION,
    .bControlInterface	   = 0,
    .bSubordinateInterface = 1,
};

const usb_cdc_descriptor_union_t usb_cdc_descriptor_union_control = {
    .bFunctionLength	   = sizeof(usb_cdc_descriptor_union_t),
    .bDescriptorType	   = USB_CDC_DESCRIPTOR_TYPE_CS_INTERFACE,
    .bDescriptorSubtype	   = USB_CDC_DESCRIPTOR_SUBTYPE_UNION,
    .bControlInterface	   = 2,
    .bSubordinateInterface = 3,
};

const usb_cdc_descriptor_acm_t usb_cdc_descriptor_acm = {
    .bFunctionLength	= sizeof(usb_cdc_descriptor_acm_t),
    .bDescriptorType	= USB_CDC_DESCRIPTOR_TYPE_CS_INTERFACE,
    .bDescriptorSubtype = USB_CDC_DESCRIPTOR_SUBTYPE_ACM,
};

const usb_descriptor_config_t usb_descriptor_config = {
    .bLength	     = sizeof(usb_descriptor_config_t),
    .bDescriptorType = USB_DESCRIPTOR_CONFIGURATION,
    .wTotalLength    = sizeof(usb_descriptor_config) + sizeof(usb_descriptor_interface_cdc[0]) +
		    sizeof(usb_cdc_descriptor_header) + sizeof(usb_cdc_descriptor_union_data) +
		    sizeof(usb_cdc_descriptor_acm) + sizeof(usb_descriptor_endpoint_cdc_data_mgmt) +
		    sizeof(usb_descriptor_interface_cdc[1]) + sizeof(usb_descriptor_endpoint_cdc_data_data) +
		    sizeof(usb_descriptor_interface_cdc[2]) + sizeof(usb_cdc_descriptor_header) +
		    sizeof(usb_cdc_descriptor_union_control) + sizeof(usb_cdc_descriptor_acm) +
		    sizeof(usb_descriptor_endpoint_cdc_control_mgmt) + sizeof(usb_descriptor_interface_cdc[3]) +
		    sizeof(usb_descriptor_endpoint_cdc_control_data),
    .bNumInterfaces	 = ARRAY_SIZE(usb_descriptor_interface_cdc),
    .bConfigurationValue = 1,
    .iConfiguration	 = 0,
    .ReservedAttr	 = true,
    .bMaxPower		 = USB_POWER_MA(100)};

usb_sendbuffer_chunks_t usb_full_configuraion_descriptor_buffer = {
    .chunk_count = 15,
    .chunks	 = {
	     {.address = (void *) &usb_descriptor_config, .size = sizeof(usb_descriptor_config)},
	     {.address = (void *) &usb_descriptor_interface_cdc[0], .size = sizeof(usb_descriptor_interface_cdc[0])},
	     {.address = (void *) &usb_cdc_descriptor_header, .size = sizeof(usb_cdc_descriptor_header)},
	     {.address = (void *) &usb_cdc_descriptor_union_data, .size = sizeof(usb_cdc_descriptor_union_data)},
	     {.address = (void *) &usb_cdc_descriptor_acm, .size = sizeof(usb_cdc_descriptor_acm)},
	     {.address = (void *) &usb_descriptor_endpoint_cdc_data_mgmt,
	      .size    = sizeof(usb_descriptor_endpoint_cdc_data_mgmt)},
	     {.address = (void *) &usb_descriptor_interface_cdc[1], .size = sizeof(usb_descriptor_interface_cdc[1])},
	     {.address = (void *) &usb_descriptor_endpoint_cdc_data_data,
	      .size    = sizeof(usb_descriptor_endpoint_cdc_data_data)},
	     {.address = (void *) &usb_descriptor_interface_cdc[2], .size = sizeof(usb_descriptor_interface_cdc[2])},
	     {.address = (void *) &usb_cdc_descriptor_header, .size = sizeof(usb_cdc_descriptor_header)},
	     {.address = (void *) &usb_cdc_descriptor_union_control, .size = sizeof(usb_cdc_descriptor_union_control)},
	     {.address = (void *) &usb_cdc_descriptor_acm, .size = sizeof(usb_cdc_descriptor_acm)},
	     {.address = (void *) &usb_descriptor_endpoint_cdc_control_mgmt,
	      .size    = sizeof(usb_descriptor_endpoint_cdc_control_mgmt)},
	     {.address = (void *) &usb_descriptor_interface_cdc[3], .size = sizeof(usb_descriptor_interface_cdc[3])},
	     {.address = (void *) &usb_descriptor_endpoint_cdc_control_data,
	      .size    = sizeof(usb_descriptor_endpoint_cdc_control_data)},
	 }};

usb_string_list_item_t usb_string_list_item_en = {.wLangID	= USB_DESCRIPTOR_LANGUAGE_ID_ENGLISH_JAMAICA,
						  .string_count = 5,
						  .strings	= {
							   [USB_DESCRIPTOR_STRING_IDX_MFR]		 = L"RED",
							   [USB_DESCRIPTOR_STRING_IDX_PRODUCT]		 = L"ECHO MACHINE",
							   [USB_DESCRIPTOR_STRING_IDX_SERIALNUMBER]	 = L"12345",
							   [USB_DESCRIPTOR_STRING_IDX_INTERFACE_DATA]	 = L"DATA",
							   [USB_DESCRIPTOR_STRING_IDX_INTERFACE_CONTROL] = L"CONTROL",
						       }};

usb_string_list_item_t usb_string_list_item_ru = {.wLangID	= USB_DESCRIPTOR_LANGUAGE_ID_RUSSIAN,
						  .string_count = 5,
						  .strings	= {
							   [USB_DESCRIPTOR_STRING_IDX_MFR]	    = L"КРАСНЫЙ",
							   [USB_DESCRIPTOR_STRING_IDX_PRODUCT]	    = L"МАШИНА ЭХО",
							   [USB_DESCRIPTOR_STRING_IDX_SERIALNUMBER] = L"12345",
							   [USB_DESCRIPTOR_STRING_IDX_INTERFACE_DATA] = L"ДАННЫЕ",
							   [USB_DESCRIPTOR_STRING_IDX_INTERFACE_CONTROL] = L"КОНТРОЛЬ",
						       }};

const usb_string_list_t usb_string_list = {.language_count = 2,
					   .langs	   = {
						&usb_string_list_item_en,
						&usb_string_list_item_ru,
					    }};
