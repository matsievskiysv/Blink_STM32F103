#include "input_parser.h"

/**
 * @brief Reset strings flags.
 *
 * @param[in] state State of the parser.
 */
static void
reset_strings_state(iprs_state *state)
{
	for (int i = 0; i < state->count; ++i) {
		state->strings[i].flags.pos  = 0;
		state->strings[i].flags.miss = 0;
	}
}

void
iprs_reset(iprs_state *state)
{
	state->flags.match	   = 0;
	state->flags.miss	   = 0;
	reset_strings_state(state);
}

void
iprs_init_state(iprs_state *state, iprs_string *strings, unsigned char strings_count, char reset, int stop_on_match,
		int autoreset)
{
	state->strings		   = strings;
	state->count		   = strings_count;
	state->reset_chr	   = reset & 0x7f;
	state->flags.autoreset	   = autoreset;
	state->flags.stop_on_match = stop_on_match;
	state->flags.use_reset_chr = !((1 << 7) & reset);
	iprs_reset(state);
}

iprs_result
iprs_process(iprs_state *state, char *buffer, unsigned char size)
{
	for (unsigned int buf_idx = 0; buf_idx < size; ++buf_idx) {
		if (state->flags.stop_on_match && state->flags.match)
			return IPRS_MATCH;
		if (state->flags.use_reset_chr && (buffer[buf_idx] == state->reset_chr)) {
			reset_strings_state(state);
			continue;
		}
		state->flags.match	= 0;
		state->flags.miss	= 0;
		int partial_match_count = 0;
		int match_count		= 0;
		for (unsigned int str_idx = 0; str_idx < state->count; ++str_idx) {
			iprs_string *str = &state->strings[str_idx];
			if (str->flags.miss)
				continue;
			char str_chr = str->string[str->flags.pos++];
			if (str_chr == '\0') {
				// do not allow '\0' to match
				str->flags.miss = 1;
			} else if (str_chr == buffer[buf_idx]) {
				state->match = str_idx;
				partial_match_count++;
				if (str->string[str->flags.pos] == '\0')
					match_count++;
			} else {
				str->flags.miss = 1;
			}
			if (str->flags.miss && state->flags.autoreset) {
				str->flags.pos	= 0;
				str->flags.miss = 0;
			}
		}
		if ((match_count >= 1) && (state->flags.stop_on_match || (buf_idx + 1 == size))) {
			state->flags.match = 1;
			return IPRS_MATCH;
		} else if ((partial_match_count == 0) && !state->flags.autoreset &&
			   (!state->flags.use_reset_chr || (buf_idx + 1 == size))) {
			state->flags.miss = 1;
			return IPRS_MISS;
		}
	}
	return IPRS_CONTINUE;
}

iprs_result
iprs_status(iprs_state *state)
{
	if (state->flags.match)
		return IPRS_MATCH;
	else if (state->flags.miss)
		return IPRS_MISS;
	else
		return IPRS_CONTINUE;
}

unsigned char
iprs_match(iprs_state *state)
{
	return state->match;
}
