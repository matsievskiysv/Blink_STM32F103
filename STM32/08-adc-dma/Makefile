# toolchain
CROSS_COMPILE := arm-none-eabi-
CC := $(CROSS_COMPILE)gcc
AS := $(CROSS_COMPILE)as
OBGDUMP := $(CROSS_COMPILE)objdump
OBGCOPY := $(CROSS_COMPILE)objcopy
NM := $(CROSS_COMPILE)nm
GDB := gdb-multiarch
OPENOCD ?= openocd

AARCH = -mcpu=cortex-m3
CCWARNINGS = -Wall -Wextra
ASWARNINGS = --fatal-warnings
ARMMODE = -mthumb
ABI = -mabi=aapcs-linux

# flags
GCCOPTS = -nostartfiles -nostdlib -ffreestanding -nodefaultlibs -fno-zero-initialized-in-bss -fstack-usage
LDFLAGS = -T$(LINKSCR) -lgcc $(GCCOPTS)
ASFLAGS = $(ARMMODE) $(AARCH)
CCFLAGS = -std=gnu2x $(GCCOPTS) $(ARMMODE) $(WARNINGS) $(AARCH) $(ABI)
INCLUDE =
INCLUDE_FILES := $(wildcard *.h)


ifdef FPU
CCFLAGS += -D__FPU_PRESENT
else
ABI += -mfloat-abi=soft
endif

ifndef NDEBUG
CCFLAGS += -O0 -ggdb
ASFLAGS += -gstabs+ -g
else
CCFLAGS += -Os
endif

# objects
TARGET := main
SRC := $(wildcard *.c *.s)
OBJS := $(patsubst %.s,%.o,$(SRC:%.c=%.o))
BIN := $(TARGET).elf
LINKSCR := $(TARGET).ld
OCD_CONF := ../openocd.cfg


$(BIN): $(OBJS) $(LINKSCR) $(INCLUDE_FILES)
	$(CC) $(CCFLAGS) -o $@ $(OBJS) $(LDFLAGS)

$(TARGET).bin: $(BIN)
	$(OBGCOPY) $< -O binary $@

%.o: %.s
	$(AS) -c $(ASFLAGS) $(ASWARNINGS) $< -o $@

%.o: %.c
	$(CC) -c $(INCLUDE) $(CCFLAGS) $(CCWARNINGS) $< -o $@

.PHONY: headers
headers: $(BIN)
	$(OBGDUMP) -h $<

.PHONY: disassemble
disassemble: $(BIN)
	$(OBGDUMP) -wDz $<

.PHONY: symbols
symbols: $(BIN)
	$(NM) $<

.PHONY: debug
debug: $(BIN)
	$(GDB) -ix ../gdbinit -tui -ex 'tui reg all' \
	-ex 'file $^' \
	-ex 'target extended-remote | $(OPENOCD) -f $(OCD_CONF)' \
	-ex 'mflash_load $^'

.PHONY: clean
clean:
	rm -f $(OBJS)
	rm -f $(BIN)
	rm -f $(TARGET).bin
	rm -f *.log
	rm -f *~
	rm -f compile_commands.json
