#pragma once

#define RCC 0x4002##1000##UL

#define RCC_CR	     0x00##UL
#define RCC_CFGR     0x04##UL
#define RCC_CIR	     0x08##UL
#define RCC_APB2RSTR 0x0C##UL
#define RCC_APB1RSTR 0x10##UL
#define RCC_AHBENR   0x14##UL
#define RCC_APB2ENR  0x18##UL
#define RCC_APB1ENR  0x1C##UL
#define RCC_BDCR     0x20##UL
#define RCC_CSR	     0x24##UL
#define RCC_AHBRSTR  0x28##UL
#define RCC_CFGR2    0x2C##UL
