#pragma once

#define GPIO_A 0x4001##0800##UL
#define GPIO_B 0x4001##0C00##UL
#define GPIO_C 0x4001##1000##UL
#define GPIO_D 0x4001##1400##UL
#define GPIO_E 0x4001##1800##UL
#define GPIO_F 0x4001##1C00##UL
#define GPIO_G 0x4001##2000##UL

#define GPIOx_CRL  0x00##UL
#define GPIOx_CRH  0x04##UL
#define GPIOx_IDR  0x08##UL
#define GPIOx_ODR  0x0C##UL
#define GPIOx_BSSR 0x10##UL
#define GPIOx_BRR  0x14##UL
#define GPIOx_LCKR 0x18##UL
