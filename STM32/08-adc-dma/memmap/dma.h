#pragma once

#define DMA1 0x4002##0000##UL
#define DMA2 0x4002##4000##UL

#define DMA_ISR	 0x00##UL
#define DMA_IFCR 0x04##UL

#define DMA_CH(n) 0xd20##UL *(n - 1)

#define DMAx_CCRx   0x08##UL
#define DMAx_CNDTRx 0x0C##UL
#define DMAx_CPARx  0x10##UL
#define DMAx_CMARx  0x14##UL
