#pragma once

#define TIM1  0x4001##2C00##UL
#define TIM2  0x4000##0000##UL
#define TIM3  0x4000##0400##UL
#define TIM4  0x4000##0800##UL
#define TIM5  0x4000##0C00##UL
#define TIM6  0x4000##1000##UL
#define TIM7  0x4000##1400##UL
#define TIM8  0x4001##3400##UL
#define TIM9  0x4001##4C00##UL
#define TIM10 0x4001##5000##UL
#define TIM11 0x4001##5400##UL
#define TIM12 0x4000##1800##UL
#define TIM13 0x4000##1C00##UL
#define TIM14 0x4000##2000##UL

#define TIMx_CR1   0x00##UL
#define TIMx_CR2   0x04##UL
#define TIMx_SMCR  0x08##UL
#define TIMx_DIER  0x0C##UL
#define TIMx_SR	   0x10##UL
#define TIMx_EGR   0x14##UL
#define TIMx_CCMR1 0x18##UL
#define TIMx_CCMR2 0x1C##UL
#define TIMx_CCER  0x20##UL
#define TIMx_CNT   0x24##UL
#define TIMx_PSC   0x28##UL
#define TIMx_ARR   0x2C##UL
#define TIMx_CCR1  0x34##UL
#define TIMx_CCR2  0x38##UL
#define TIMx_CCR3  0x3C##UL
#define TIMx_CCR4  0x40##UL
#define TIMx_DCR   0x48##UL
#define TIMx_DMAR  0x4C##UL
