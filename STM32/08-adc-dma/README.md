# DMA
In our previous example we took a straightforward approach of doing all operations in the foreground: we started the conversion manually and waited for the results by busy-looping until the conversion flag was raised. This approach is fine for the small programs where micro-controller only has one task but generally it's better to avoid busy-loops and do tasks in the background where possible.
In case of ADC there are two ways we could offload tasks in the background. The first one is to use interrupts. In this particular case this approach is limited due to the ADC internal structure: ADC can be programmed to do a number of conversions automatically but the result of each conversion will be stored in the same `ADCx_DR` register, overwriting previously obtained results; interrupt will be raised only after conversion of the whole group.
The only way to preserve conversion data of the whole group is to take DMA (direct memory access) approach. In this case after each conversion ADC will request DMA controller to copy data from `ADCx_DR` register to the predefined address. DMA controller can be configured to increment this predefined address after each transfer so that conversion data is preserved.

## DMA configuration

DMA transfers data from peripheral register to the RAM, therefore we need to allocate buffer for ADC data. This is done by defining global array of 16 bit values.

```c
unsigned short int temp_sens_raw[2] = {0};
```

Address of this array will be set as DMA destination address. In circular mode DMA will fill memory region with conversion group data and wrap around to the initial address. So, if we set DMA transfer and ADC conversion group sizes to be equal, then each conversion data will always be written to the same RAM address.

DMA controller has a number of channels which are capable to receive DMA requests from specified peripherals. In our case only channel 1 may communicate with ADC.
Configuration steps for DMA are:
1. enable DMA clock;
1. disable selected DMA channel before configuration;
1. set transfer chunk sizes, enable circular mode, memory auto-increment, interrupts;
1. set peripheral register address;
1. set memory address;
1. set number of transmissions before returning to the start address (in circular mode);
1. enable channel and global interrupt.

```c
// enable DMA1 clock
volatile unsigned long *const rcc_ahbenr = (unsigned long *) (RCC + RCC_AHBENR);
*rcc_ahbenr |= (1 << 0);
volatile unsigned long *const dma1_ccr1 = (unsigned long *) (DMA1 + DMA_CH(1) + DMAx_CCRx);
// disable channel 1
*dma1_ccr1 &= ~(1 << 0); // EN
// configure channel 1
*dma1_ccr1 |= (0b01 << 12)   // PL
          | (0b01 << 10)     // MSIZE
          | (0b01 << 8)      // PSIZE
          | (1 << 7)         // MINC
          | (1 << 5)         // CIRC
          | (1 << 3)         // TEIE
          | (1 << 1);        // TCIE
// set peripheral address to ADC1 data register
volatile unsigned long *const dma1_cpar1 = (unsigned long *) (DMA1 + DMA_CH(1) + DMAx_CPARx);
*dma1_cpar1              = ADC1 + ADCx_DR;
// set memory address to our variable
volatile unsigned long *const dma1_cmar1 = (unsigned long *) (DMA1 + DMA_CH(1) + DMAx_CMARx);
*dma1_cmar1              = (unsigned long) &temp_sens_raw;
// set transfer size
volatile unsigned long *const dma1_cndtr1 = (unsigned long *) (DMA1 + DMA_CH(1) + DMAx_CNDTRx);
*dma1_cndtr1                  = 2;
// enable DMA interrupt (#11)
volatile unsigned long *const nvic_iser_tim2 = (unsigned long *) (NVIC_ISER + NVIC_REG_OFFSET(11));
*nvic_iser_tim2 |= NVIC_BIT(11);
// enable channel 1
*dma1_ccr1 |= (1 << 0); // EN
```

## ADC configuration

ADC also has to be configured to use DMA. In previous example we configured ADC before each conversion of temperature sensor output and reference voltage source. Now we can configure ADC to do these conversions automatically.
Configuration steps for ADC become:
1. enable ADC clock;
1. configure ADC clock prescaler;
1. put ADC in scan mode, enable ADC, temperature sensor, reference voltage source;
1. configure ADC to request DMA;
1. set sample time for channels 16 and 17;
1. set conversion group size to 2;
1. put channels 16 and 17 in the beginning of conversion queue;

```c
// enable ADC1 clock
volatile unsigned long *const rcc_apb2enr = (unsigned long *) (RCC + RCC_APB2ENR);
*rcc_apb2enr |= (1 << 9);
// set ADC1 clock prescaler 1/8
volatile unsigned long *const rcc_cfgr = (unsigned long *) (RCC + RCC_CFGR);
*rcc_cfgr |= (0b11 << 14);
// setup ADC1
volatile unsigned long *const adc_cr1 = (unsigned long *) (ADC1 + ADCx_CR1);
*adc_cr1 |= (1 << 8); // SCAN
volatile unsigned long *const adc_cr2 = (unsigned long *) (ADC1 + ADCx_CR2);
*adc_cr2 |= (1 << 23)   // TSVREFE
        | (1 << 8)      // DMA
        | (1 << 0);     // ADON
// setup ADC1 maximum sample time
volatile unsigned long *const adc_smpr1 = (unsigned long *) (ADC1 + ADCx_SMPR1);
*adc_smpr1 |= (0b111 << 21) | (0b111 << 18);
// set ADC1 sequence length
volatile unsigned long *const adc_sqr1 = (unsigned long *) (ADC1 + ADCx_SQR1);
*adc_sqr1 |= (1 << 20);
// setup ADC1 sequence
volatile unsigned long *const adc_sqr3 = (unsigned long *) (ADC1 + ADCx_SQR3);
*adc_sqr3 |= (16 << 0) | (17 << 5);
```

## DMA interrupt handler

DMA can be configured to generate interrupts on half transfer, full transfer and transfer error. We are not interested in half transfers and did not enable this interrupt. In our interrupt handler we convert raw ADC readings to actual temperature like in previous example. In case of transfer error we set `temp` to `-273`.
We also need to clear DMA interrupt flags.

```c
void
dma1_channel1_irqhandler(void)
{
    volatile unsigned long *const dma1_isr = (unsigned long *) (DMA1 + DMA_ISR);
    if (*dma1_isr & (1 << 3)) {
        // handle transfer error
        temp = -273;
    } else {
    #ifdef __FPU_PRESENT
        temp = (1.43 - (1.2 * temp_sens_raw[0]) / temp_sens_raw[1]) / 0.0043 * 1000 + 25000;
    #else
        temp = (14300 - (12000L * temp_sens_raw[0]) / temp_sens_raw[1]) * 1000 / 43 + 25000;
    #endif
    }
    volatile unsigned long *const dma1_ifcr = (unsigned long *) (DMA1 + DMA_IFCR);
    *dma1_ifcr                              = 0x1f; // reset interrupts
}
```

## Timer

We could configure ADC to start conversion on one of the internal timers but we don't need to measure so frequently. Also, from time to time we want to recalibrate our ADC to account for temperature drift. This could be implemented using timer with callbacks. We will use `systick` as a timer.
We introduce structure
```c
struct job_list_t {
    unsigned short int count;
    unsigned short int current;
    void (*callback)(void);
};
```
This structure contains `callback` pointer to a function. This function will be called after a `count` of `systick` cycles is elapsed. `current` field keeps track of the current cycle value.

Now we put our jobs in the `job_list_t` array.
```c
struct job_list_t job_list[] = {
   {.count = 60, .current = 0, .callback = calibrate_temp_sensor},
   {.count = 10, .current = 0, .callback = start_temp_sensor},
   {0},
};
```
First element is a `calibrate_temp_sensor` function structure, which will be executed each 60th cycle and calibrate out ADC. Second element is a `start_temp_sensor` function structure, which will be executed each 10th cycle and start conversion. Array is terminated by `0` so that we will know when to stop iterating over array.
Our counter function is straightforward. It is placed into `systick_handler` so that it is called each time `systick` reaches zero (i.e. each second).
```c
void
systick_handler(void)
{
	for (unsigned int i = 0; job_list[i].callback != NULL; i++) {
		if (job_list[i].current == 0) {
			job_list[i].callback();
			job_list[i].current = job_list[i].count;
		} else {
			job_list[i].current--;
		}
	}
}
```

And the actual callback functions are very simple.
```c
void
calibrate_temp_sensor(void)
{
    volatile unsigned long *const adc_cr2 = (unsigned long *) (ADC1 + ADCx_CR2);
    // Start calibration
    *adc_cr2 |= (1 << 2);
    while (*adc_cr2 & (1 << 2)) {} // wait until complete
}
```

```c
void
start_temp_sensor(void)
{
    volatile unsigned long *const adc_sr  = (unsigned long *) (ADC1 + ADCx_SR);
    volatile unsigned long *const adc_cr2 = (unsigned long *) (ADC1 + ADCx_CR2);
    *adc_sr &= ~0x1f;     // reset status register
    *adc_cr2 |= (1 << 0); // Start conversion
}
```

As you can see, this timer with callbacks approach is convenient and very simple to implement. But it is has limitations, which should be kept in mind:
* callbacks must use global variables to store data;
* callbacks are executed in interrupt context, therefore have to be short;
* long background tasks cannot be placed in callbacks.
