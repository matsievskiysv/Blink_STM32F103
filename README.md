This repository contains a bare-metal ARM project for STM32F103 chip. It is build with GNU tool chain whit use of OpenOCD debugger (stlink interface) and is kept is simple is possible.

List of parts:

1. [shell](./shell) -- basic shell functions
1. [arm-assembly](./arm-assembly) -- introduction to ARM assembly using [QEMU](https://www.qemu.org/) emulator
1. [STM32](./STM32) -- introduction to bare-metal
ARM using Cortex-M3 based STM32 micro-controller
